RCOMP 2021-2022 Project - Sprint 2 planning
===========================================
### Sprint master: 2222222 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
all the members of Sprint will configure the building that was assigned to you in the previous sprint.
This configuration will go from the connection of IC, CP, PC, creation of Vlan's, to the introduction of their Ip's in the devices as well as the correspondence of the correct VLAN
* 
* **T.1.1** Development of a layer 2 and 3 configuration for building , and also encompassing the campus backbone. Integration of every members’ Packet Tracer simulations into a single simulation.
* **T.1.2** Development of a layer 2 and 3 configuration for building 2.
* **T.1.3** Development of a layer 2 and 3 configuration for building 3.
* **T.1.4** Development of a layer 2 and 3 configuration for building 4.

# 2. Technical decisions and coordination #
For the names and hostnames it was established the standard to call them for example HC_[Room] where the Room represents the room number, because this room number already has the name of the building and the floor.
example HC_301- corresponds to the HC that is in building 3, floor 0 in room 1.

### Models of equipment ###
**Router**- 2811 model

**Switches**- Pt-empty switch model

**VoIP phone** - 7960 model

####VLAN Ids####

For the Vlan representation we decided that the nomenclature associated with these would be [Edif.]_VlanName where EDIF. represents the building

| VLANID's   range to be used | 330-360  |
|:---------------------------:|:---------|
|            Name             | VLAN ID  |
|         0_BackBone          | 330      |
|         1_EndUser_0         | 331      |
|         1_EndUser_1         | 332      |
|           1_WIFI            | 333      |
|            1_DMZ            | 334      |
|           1_VoIP            | 335      |
|         2_EndUser_0         | 336      |
|         2_EndUser_1         | 337      |
|           2_WIFI            | 338      |
|            2_DMZ            | 339      |
|           2_VoIP            | 340      |
|         3_EndUser_0         | 341      |
|         3_EndUser_1         | 342      |
|           3_WIFI            | 343      |
|            3_DMZ            | 344      |
|           3_VoIP            | 345      |
|         4_EndUser_0         | 346      |
|         4_EndUser_1         | 347      |
|           4_WIFI            | 348      |
|            4_DMZ            | 349      |
|           4_VoIP            | 350      |

#### IPv4 networks' addresses and routers' addresses ####

BackBone  : 120 nodes

Building 1: 292 nodes

Building 2: 219 nodes

Building 3: 188 nodes

Building 4: 175 nodes

| Building |  IP  Start Address  |  IP End Address   |
|:--------:|:-------------------:|:-----------------:|
|    1     |   172.17.112.0/23   |172.12.114.64/26	  |
|    2     |  172.12.114.128/25  | 172.12.115.112/28 |
|    3     |  172.12.115.128/26  | 172.12.116.96/27  |
|    4     |  172.12.116.128/25  | 172.12.117.112/28 |


Router: 15.203.47.165/30
Mask : 255.255.255.252

# 3. Subtasks assignment #
All the members of the project will 

#### Example: ####D
  * 1201623 - Building 1
  * 1191706 - Building 4
  * 1191568 - Building 3
  * 1200735 - Building 2

