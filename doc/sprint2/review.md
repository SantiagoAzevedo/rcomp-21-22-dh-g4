RCOMP 2021-2022 Project - Sprint 2 review
=========================================
### Sprint master: 1191568 ###

# 1. Sprint's backlog #
* **T.1.1** Development of a layer 2 and 3 configuration for building , and also encompassing the campus backbone. Integration of every members’ Packet Tracer simulations into a single simulation.
* **T.1.2** Development of a layer 2 and 3 configuration for building 2.
* **T.1.3** Development of a layer 2 and 3 configuration for building 3.
* **T.1.4** Development of a layer 2 and 3 configuration for building 4.
# 2. Subtasks assessment #

## 2.1. 1201623 - Structured cable design for building 1 #
### Totally implemented without issues. ###
## 2.2. 1200735 - Structured cable design for building 2 #
### Totally implemented without issues. ###
## 2.3. 1191568 - Structured cable design for building 3 #
### Totally implemented without issues. ###
## 2.4. 1191706 - Structured cable design for building 4 #
### Totally implemented without issues. ###
