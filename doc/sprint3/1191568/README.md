RCOMP 2021-2022 Project - Sprint 3 - Member 1191568 folder
===========================================

# RCOMP 2021-2022 Sprint 3

O presente documento pretende expor como ocorreu a alteração de routing tables estáticas para dinâmicas com o OSPF, a implementação do servidor HTTP, do serviço DHCP e VoIP , o estabelecimento de dominios DNS e finalmente a implementação do NAT(Network Address Translation) e das firewall estáticas(ACLs).

## Building 2 ##

Ao testar pings entre diferentes dispositivos e diferentes edifícios é normal que as primeiras tentativas falhem e devido ao tamanho da simulação é necessário esperar algum tempo antes de se testar os mesmos.

## 1. OSPF

De modo a garantir uma correta simulação, o OSPF foi configurado em cada router do edifício, respeitando as áreas definidas no planning.
Para a configuração foram utilizados os ip's referentes a cada edifício:

> 3_WIFI: network 172.12.115.129 0.0.0.63 area 3
>
> 3_EndUser_1: network 172.12.116.1 0.0.0.63 area 3
>
> 3_EndUser_0: network 172.12.115.193 0.0.0.63 area 3
>
> 3_DMZ: network 172.12.116.65 0.0.0.31 area 3
>
> Backbone: network 172.17.112.4 0.0.0.127 area 0

## 2. Servidor HTTP

Foi adicionada um servidor em cada rede DMZ e foi-lhe definido um ip: 172.12.116.67 , depois foi ativado o serviço HTTP e criada uma página HTML.

![http.png](imagens/http.png)

## 3. DHCP

No router foi configurado o serviço DHCP para VoIP, WiFi e para os dois end users (piso 0 e piso 1). O nome de cada dhcp está definido no comando "ip dhcp pool (...)", em que (...) corresponde ao nome do respetivo dhcp sendo este o mesmo nome da VLAN para nao haver erros.
A cada dhcp, há um endereço ipv4 correspondente. Se se analisar o sprint anterior, repara-se que a cada VLAN existe também um endereço, e este endereço corresponde à network do dhcp.
É de destacar também que existe um nome do domain, "building-3.rcomp-20-21-dh-g4", correspondente ao grupo de trabalho.
Em todos os dhcp existe um default router correspondente.

    ip dhcp pool 3_EndUser_1
     network 172.12.116.0 255.255.255.192
     default-router 172.12.116.1
     dns-server 172.12.116.66
     domain-name building-3.rcomp-21-22-dh-g4

    ip dhcp pool 3_EndUser_0
     network 172.12.115.192 255.255.255.192
     default-router 172.12.115.193
     dns-server 172.12.116.66
     domain-name building-3.rcomp-21-22-dh-g4

    ip dhcp pool 3_VoIP
     network 172.12.116.96 255.255.255.224
     default-router 172.12.116.97

     option 150 ip 172.12.116.97
     dns-server 172.12.116.66
     domain-name building-3.rcomp-21-22-dh-g4
    ip dhcp pool 3_WIFI
     network 172.12.115.128 255.255.255.192

## VoIP

A configuração dos telefones teria que ter o source-address correspondente ao primeiro endereço válido da VLAN dos VoIP (172.12.116.97) e também o port 2000, que usa o protocolo para comunicação na network.

    telephony-service
    max-ephones 2
    max-dn 2
    ip source-address 172.12.116.97 port 2000

Foram definidos dois telefones no edifício, sendo que cada um tem um line number correspondente a 3000 e 3001.

    ephone-dn 1
    number 3000
    !
    ephone 1
    device-security-mode none
    mac-address 000B.BEED.3D5B
    type 7960
    button 1:1
    !
    ephone-dn 2
    number 3001
    !
    ephone 2
    device-security-mode none
    mac-address 0090.21B9.68A5
    type 7960
    button 1:2

Repare-se que em cada ephone, existe um mac-address correspondente. Apesar de existirem outras maneiras de configurar os ip phones, ao especificar o mac address manualmente, conseguiu-se resolver um erro constante na configuração dos VoIPs do edifício  " _Configuring IP_ " que fazia com que cada telefone nunca conseguisse ter um IP associado, e por sua vez, nunca tinha um line number.
Note-se que ainda fizemos com que os telefones ligam-se entre edifícios. 

    dial-peer voice 1 voip
    destination-pattern 1...
    session target ipv4:172.12.114.65
    !
    dial-peer voice 2 voip
    destination-pattern 2...
    session target ipv4:172.12.115.113
    !
    dial-peer voice 4 voip
    destination-pattern 4...
    session target ipv4:172.12.117.97

![3000_3001.png](imagens/3000_3001.png)

![3000_1001.png](imagens/3000_1001.png)

## DNS

Para configurar o dominio DNS no edifício 3 foi necessário no servidor DNS criar a seguinte tabela:

![dnsTable.png](imagens/dnsTable.png)

A presente tabela apresenta A Records e NS records para o root, que é o name server em questão, e para os name servers existentes em cada edifício. Para além disso apresenta um A Record e CNAME para o server 1, que corresponde ao servidor HTTP/HTTPS existente no edifício 3.

## NAT(Network Address Translation)

De modo a redirecionar os pedidos de HTTP e HTTPS para o servidor HTTP no DMZ local no router foi feita a seguinte configuração utilizando as portas 80 e 443 como default.

Todos os pedidos DNS recebido são também redirecionados do router da backbone para o servidor local de DNS sendo a o número da porta default a 53.

A configuração apresentada irá ser aplicada a pacotes provenientes do servidor DNS e servidor HTTP/HTTPS existentes na rede DMZ. De modo a ocultar o ip address a redes externas do edifício, os pacotes que tenham como destino uma dessas, terão o seu ip alterado no router, para o ip do router para a VLAN do Backbone. O oposto irá ocorrer.
Com isto, um end node que tente aceder ao servidor HTTP, terá de ir pelo endereço do router do edifício 1 para a VLAN do backbone.
<br />
<br />
![NATtable.png](imagens/NATtable.png)


## Static Firewalls (ACLs)

Considera-se que todas as ACLs serão aplicadas ao incoming traffic no router, para cada VLAN.

#### VLAN WI-FI

    access-list 102 permit ospf any any
    access-list 102 permit udp any any eq 67
    access-list 102 permit udp any any eq 68
    access-list 102 permit tcp 172.12.115.128 0.0.0.127 host 172.12.116.67 eq 443
    access-list 102 deny tcp any 172.12.116.64 0.0.0.15 eq 80
    access-list 102 permit tcp 172.12.115.128 0.0.0.127 any eq 80
    access-list 102 deny tcp any 172.12.116.64 0.0.0.15 eq 443
    access-list 102 permit tcp 172.12.115.128 0.0.0.127 any eq 443
    
    access-list 102 permit udp 172.12.115.128 0.0.0.127 host 172.12.116.66 eq 53
    access-list 102 permit tcp 172.12.115.128 0.0.0.127 host 172.12.116.66 eq 53
    access-list 102 permit icmp 172.12.115.128 0.0.0.127 any echo
    access-list 102 permit icmp 172.12.115.128 0.0.0.127 any echo-reply
    access-list 102 deny ip any host 172.12.116.65


#### VLAN END USERS PISO 0

    access-list 100 permit ospf any any
    access-list 100 permit udp any any eq 67
    access-list 100 permit udp any any eq 68    access-list 100 permit tcp 172.12.115.192 0.0.0.31 host 172.12.116.67 eq 80
    access-list 100 permit tcp 172.12.115.192 0.0.0.31 host 172.12.116.67 eq 443
    access-list 100 deny tcp any 172.12.116.64  0.0.0.15 eq 80
    access-list 100 permit tcp 172.12.115.192 0.0.0.31 any eq 80
    access-list 100 deny tcp any 172.12.116.64  0.0.0.15 eq 443
    access-list 100 permit tcp 172.12.115.192 0.0.0.31 any eq 443   access-list 100 permit udp 172.12.115.192 0.0.0.31 host 172.12.116.66 eq 53
    access-list 100 permit tcp 172.12.115.192 0.0.0.31 host 172.12.116.66 eq 53
    access-list 100 permit icmp 172.12.115.192 0.0.0.31 any echo
    access-list 100 permit icmp 172.12.115.192 0.0.0.31 any echo-reply  
    access-list 100 deny ip any host 172.12.115.193


#### VLAN END USERS PISO 1

    access-list 101 permit ospf any any
    access-list 101 permit udp any any eq 67
    access-list 101 permit udp any any eq 68
    access-list 101 permit tcp 172.12.116.0 0.0.0.63 host 172.12.116.67 eq 443
    access-list 101 deny tcp any 172.12.116.64 0.0.0.15 eq 80
    access-list 101 permit tcp 172.12.116.0 0.0.0.63 any eq 80
    access-list 101 deny tcp any 172.12.116.64 0.0.0.15 eq 443
    access-list 101 permit tcp 172.12.116.0 0.0.0.63 any eq 443

							// IP Server DNS    access-list 101 permit tcp 172.12.116.0 0.0.0.63 host 172.12.116.66 eq 53
    access-list 101 permit icmp 172.12.116.0 0.0.0.63 any echo
    access-list 101 permit icmp 172.12.116.0 0.0.0.63 any echo-reply
    access-list 101 deny ip any host 172.12.116.1


#### VLAN VoIP

    access-list 103 permit ospf any any
    access-list 103 permit udp any any eq 67
    access-list 103 permit udp any any eq 68    
    access-list 103 permit udp 172.12.116.96 0.0.0.15 host 172.12.115.113 eq 69
    access-list 103 permit tcp 172.12.116.96 0.0.0.15 host 172.12.115.113 eq 2000
    access-list 103 permit ip 172.12.116.96 0.0.0.15 any

#### VLAN BACKBONE


    access-list 104 deny ip 172.12.115.128 0.0.0.31 any
    access-list 104 deny ip 172.12.116.0 0.0.0.63 any
    access-list 104 deny ip 172.12.115.192 0.0.0.127 any
    access-list 104 deny ip 172.12.116.64 0.0.0.15 any
    access-list 104 deny ip 172.12.116.96 0.0.0.15 any
    access-list 104 permit udp any host 172.12.112.4 eq 53
    access-list 104 permit tcp any host 172.12.112.4 eq 53
    access-list 104 permit tcp any host 172.12.112.4 eq 80
    access-list 104 permit tcp any host 172.12.112.4 eq 443
    access-list 104 permit tcp any host 172.12.112.4 eq 2000
    access-list 104 permit ospf any any
    access-list 104 permit icmp any any echo
    access-list 104 permit icmp any any echo-reply
    access-list 104 deny ip any host 172.12.112.4
    access-list 104 permit ip any any


### Associar ACL

Piso 0:

    interface FastEthernet1/0.1
    ip access-group 100 in

Piso 1:

    interface FastEthernet1/0.2
    ip access-group 101 in

Wifi:

    interface FastEthernet1/0.3
    ip access-group 102 in


VoIP:

    interface FastEthernet1/0.5
    ip access-group 103 in

Backbone:
A ACL relativa ao backbone nao foi implementada devido a problemas. Quando a adicionavamos os pc's deixavam de puder pingar www e server1 por isso achamos melhor nao a implementar pois um dos objetivos do trabalho seria pingar www e aceder à pagina

