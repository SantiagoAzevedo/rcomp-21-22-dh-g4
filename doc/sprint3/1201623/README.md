RCOMP 2021-2022 Project - Sprint 3 - Member 1201623 folder
===========================================

# RCOMP 2021-2022 Sprint 3

This document intends to expose how static routing tables changed for dynamics with OSPF, the implementation of HTTP server, dhcp and voip service, the establishment of DNS domains and finally the implementation of NAT (Network Address Translation) and static firewalls (ACLs). 

## Building 1 ##

## 1. OSPF

In order to ensure a correct simulation, the OSPF was configured on each router of the building, respecting the areas defined in the planning. 
OSPF config:

> 0_BackBone: network 172.12.112.0 0.0.0.127 area 0
> 
> 1_EndUser_0: network 172.12.114.0 0.0.0.63 area 1
> 
> 1_EndUser_1: network 172.12.112.128 0.0.0.127 area 1
> 
> 1_WIFI: network 172.12.113.0 0.0.0.127 area 1
> 
> 1_DMZ: network 172.12.113.128 0.0.0.127 area 1
> 
> 1_VoIP: network 172.12.114.64 0.0.0.63 area 1

## 2. HTTP server

A server was added to the DMZ of this building and an ip was set: 172.12.113.131, then the HTTP service was activated and an HTML page was created.

![http.png](images/HTTP.PNG)

## 3. DHCP

On the router was configured the DHCP service for VoIP, WiFi and for the two end users (floor 0 and floor 1).
With each dhcp, there is a corresponding ipv4 address. If you look at the previous sprint, it is note that each VLAN also has an address, and this address corresponds to the dhcp network.  

    ip dhcp excluded-address 172.12.112.129
    ip dhcp excluded-address 172.12.113.1
    ip dhcp excluded-address 172.12.114.1
    ip dhcp excluded-address 172.12.114.65

    ip dhcp pool 1_EndUser_0
     network 172.12.114.0 255.255.255.192
     default-router 172.12.114.1
     dns-server 172.12.113.130
     domain-name rcomp-21-22-dh-g4

    ip dhcp pool 1_EndUser_1
     network 172.12.112.128 255.255.255.128
     default-router 172.12.112.129
     dns-server 172.12.113.130
     domain-name rcomp-21-22-dh-g4

    ip dhcp pool 1_WIFI
     network 172.12.113.0 255.255.255.128
     default-router 172.12.113.1
     dns-server 172.12.113.130
     domain-name rcomp-21-22-dh-g4

    ip dhcp pool 1_VoIP
     network 172.12.114.64 255.255.255.192
     default-router 172.12.114.65
     option 150 ip 172.12.114.65
     dns-server 172.12.113.130
     domain-name rcomp-21-22-dh-g4

## VoIP

The configuration of the phones would have to have the source-address corresponding to the first valid VOIP VLAN address (172.12.114.65) and also port 2000, which uses the protocol for communication on the network. 

    telephony-service
     max-ephones 2
     max-dn 2
     ip source-address 172.12.114.65 port 2000
     auto assign 1 to 2

Two phones were defined in the building, each of which has a line number corresponding to 1000 and 1001. 

    ephone-dn 1
     number 1000

    ephone-dn 2
     number 1001

    ephone 1
     device-security-mode none
     mac-address 00D0.9789.5051
     type 7960
     button 1:1

    ephone 2
     device-security-mode none
     mac-address 0040.0BAA.7577
     type 7960
     button 1:2

To connect with phones from other buildings, it's necessary to add: 

    dial-peer voice 1 voip
     destination-pattern 2...
     session target ipv4:172.12.115.113

    dial-peer voice 2 voip
     destination-pattern 3...
     session target ipv4:172.12.116.97

    dial-peer voice 3 voip
     destination-pattern 4...
     session target ipv4:172.12.117.97


## DNS

Para configurar o dominio DNS no edifício 3 foi necessário no servidor DNS criar a seguinte tabela:

![dnsTable.png](images/dnsTable.PNG)

This table presents A Records and NS records for building 2, 3 and 4. It also presents an A Record and CNAME for server 1, which corresponds to the existing HTTP/HTTPS server in building 1. 

## NAT(Network Address Translation)

In order to redirect the HTTP and HTTPS requests to the HTTP server on the local DMZ on the router the following configuration was made using the ports 80 and 443 as default.

All dns requests received are also redirected from the backbone router to the local DNS server being the default port number to 53. 

![NATtable.png](images/NATtable.PNG)

## ACL

It was decided not to assign one access list to router interfaces as the assignment caused an error that disabled some features. The VLAN BACKBONE make conflicts with telephone connections between buildings.

#### VLAN END USERS FLOOR 0

1. access-list 100 permit ospf any any
2. access-list 100 permit udp any any eq 67
3. access-list 100 permit udp any any eq 68
4. access-list 100 permit tcp 172.12.114.0 0.0.0.63 host 172.12.113.131 eq 80
5. access-list 100 permit tcp 172.12.114.0 0.0.0.63 host 172.12.113.131 eq 443
6. access-list 100 deny tcp any 172.12.113.128 0.0.0.15 eq 80
7. access-list 100 permit tcp 172.12.114.0 0.0.0.63 any eq 80
8. access-list 100 deny tcp any 172.12.113.128 0.0.0.15 eq 443
9. access-list 100 permit tcp 172.12.114.0 0.0.0.63 any eq 443
10. access-list 100 permit udp 172.12.114.0 0.0.0.63 host 172.12.113.130 eq 53
11. access-list 100 permit tcp 172.12.114.0 0.0.0.63 host 172.12.113.130 eq 53
12. access-list 100 permit icmp 172.12.114.0 0.0.0.63 any echo
13. access-list 100 permit icmp 172.12.114.0 0.0.0.63 any echo-reply
14. access-list 100 deny ip any host 172.12.114.1

        
        1 - Allows traffic relative to OSPF.
        2 e 3 - Allow dhcp to function. 
        4 - Allows HTTP access to server1 on the DMZ. 
        5 - Allows HTTPS access to server1 on the DMZ. 
        6 e 7 - Allows HTTP access to any point on the network unless it belongs to the DMZ. 
        8 e 9 - Allows HTTP access to any point on the network unless it belongs to the DMZ. 
        10 - Allows access to the existing DNS server on the DMZ network via UDP. 
        11 - Allows access to the existing DNS server on the DMZ network via TCP. 
        12 - Allows you to send echo requests to any point in the network. 
        13 - Allows you to send echo replies to any point in the network. 
        14 - Prevents any traffic from being directed directly to the router. 



#### VLAN END USERS FLOOR 1

1. access-list 101 permit ospf any any
2. access-list 101 permit udp any any eq 67
3. access-list 101 permit udp any any eq 68
4. access-list 101 permit tcp 172.12.112.128 0.0.0.127 host 172.12.113.131 eq 80
5. access-list 101 permit tcp 172.12.112.128 0.0.0.127 host 172.12.113.131 eq 443
6. access-list 101 deny tcp any 172.12.113.128 0.0.0.15 eq 80
7. access-list 101 permit tcp 172.12.112.128 0.0.0.127 any eq 80
8. access-list 101 deny tcp any 172.12.113.128 0.0.0.15 eq 443
9. access-list 101 permit tcp 172.12.112.128 0.0.0.127 any eq 443
10. access-list 101 permit udp 172.12.112.128 0.0.0.127 host 172.12.113.130 eq 53
11. access-list 101 permit tcp 172.12.112.128 0.0.0.127 host 172.12.113.130 eq 53
12. access-list 101 permit icmp 172.12.112.128 0.0.0.127 any echo
13. access-list 101 permit icmp 172.12.112.128 0.0.0.127 any echo-reply
14. access-list 101 deny ip any host 172.12.112.129


        1 - Allows traffic relative to OSPF.
        2 e 3 - Allow dhcp to function. 
        4 - Allows HTTP access to server1 on the DMZ. 
        5 - Allows HTTPS access to server1 on the DMZ. 
        6 e 7 - Allows HTTP access to any point on the network unless it belongs to the DMZ. 
        8 e 9 - Allows HTTP access to any point on the network unless it belongs to the DMZ. 
        10 - Allows access to the existing DNS server on the DMZ network via UDP. 
        11 - Allows access to the existing DNS server on the DMZ network via TCP. 
        12 - Allows you to send echo requests to any point in the network. 
        13 - Allows you to send echo replies to any point in the network. 
        14 - Prevents any traffic from being directed directly to the router.
 


#### VLAN WIFI

1. access-list 102 permit ospf any any
2. access-list 102 permit udp any any eq 67
3. access-list 102 permit udp any any eq 68
4. access-list 102 permit tcp 172.12.113.0 0.0.0.127 host 172.12.113.131 eq 80
5. access-list 102 permit tcp 172.12.113.0 0.0.0.127 host 172.12.113.131 eq 443
6. access-list 102 deny tcp any 172.12.113.128 0.0.0.15 eq 80
7. access-list 102 permit tcp 172.12.113.0 0.0.0.127 any eq 80
8. access-list 102 deny tcp any 172.12.113.128 0.0.0.15 eq 443
9. access-list 102 permit tcp 172.12.113.0 0.0.0.127 any eq 443
10. access-list 102 permit udp 172.12.113.0 0.0.0.127 host 172.12.113.130 eq 53
11. access-list 102 permit tcp 172.12.113.0 0.0.0.127 host 172.12.113.130 eq 53
12. access-list 102 permit icmp 172.12.113.0 0.0.0.127 any echo
13. access-list 102 permit icmp 172.12.113.0 0.0.0.127 any echo-reply
14. access-list 102 deny ip any host 172.12.113.1
 

        1 - Allows traffic relative to OSPF.
        2 e 3 - Allow dhcp to function. 
        4 - Allows HTTP access to server1 on the DMZ. 
        5 - Allows HTTPS access to server1 on the DMZ. 
        6 e 7 - Allows HTTP access to any point on the network unless it belongs to the DMZ. 
        8 e 9 - Allows HTTP access to any point on the network unless it belongs to the DMZ. 
        10 - Allows access to the existing DNS server on the DMZ network via UDP. 
        11 - Allows access to the existing DNS server on the DMZ network via TCP. 
        12 - Allows you to send echo requests to any point in the network. 
        13 - Allows you to send echo replies to any point in the network. 
        14 - Prevents any traffic from being directed directly to the router. 



#### VLAN VoIP

1. access-list 103 permit ospf any any
2. access-list 103 permit udp any any eq 67
3. access-list 103 permit udp any any eq 68
4. access-list 103 permit udp 172.12.114.64 0.0.0.63 host 172.12.114.65 eq 69
5. access-list 103 permit tcp 172.12.114.64 0.0.0.63 host 172.12.114.65 eq 2000
6. access-list 103 permit ip 172.12.114.64 0.0.0.63 any


        1 - Allows traffic relative to OSPF.
        2 e 3 - Allow traffic relative to DHCP. 
        4 e 5 - They allow traffic relative to the configuration of ip phones. 
        6 - They allow any traffic to any node. 



#### VLAN BACKBONE

1. access-list 104 deny ip 172.12.113.0 0.0.0.127 any
2. access-list 104 deny ip 172.12.114.0 0.0.0.63 any
3. access-list 104 deny ip 172.12.112.128 0.0.0.127 any
4. access-list 104 deny ip 172.12.113.130 0.0.0.127 any
5. access-list 104 deny ip 172.12.114.64 0.0.0.63 any
6. access-list 104 permit udp any host 172.12.112.2 eq 53
7. access-list 104 permit tcp any host 172.12.112.2 eq 53
8. access-list 104 permit tcp any host 172.12.112.2 eq 80
9. access-list 104 permit tcp any host 172.12.112.2 eq 443
10. access-list 104 permit tcp any host 172.12.112.2 eq 2000
11. access-list 104 permit ospf any any
12. access-list 104 permit icmp any any echo
13. access-list 104 permit icmp any any echo-reply
14. access-list 104 deny ip any host 172.12.112.2
15. access-list 104 permit ip any any
 

        1 to 5 - Prevent external spoofing. 
        6 - Allows DNS traffic in UDP. 
        7 - Allows DNS traffic in TCP. 
        8 - Allows HTTP access. 
        9 - Allows HTTPS access. 
        10 - Allows access to IP Phones. 
        11 - Allows OSPF-related traffic to any node. 
        12 - Allows ICMP traffic. 
        13 - Allows ICMP response. 
        14 - Prevents traffic directed to the router. 
        15 - Allows any traffic to any node. 