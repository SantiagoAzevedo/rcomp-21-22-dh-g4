!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
ip dhcp excluded-address 172.12.112.129
ip dhcp excluded-address 172.12.113.1
ip dhcp excluded-address 172.12.114.1
ip dhcp excluded-address 172.12.114.65
!
ip dhcp pool 1_EndUser_0
 network 172.12.114.0 255.255.255.192
 default-router 172.12.114.1
 dns-server 172.12.113.130
 domain-name rcomp-21-22-dh-g4
ip dhcp pool 1_EndUser_1
 network 172.12.112.128 255.255.255.128
 default-router 172.12.112.129
 dns-server 172.12.113.130
 domain-name rcomp-21-22-dh-g4
ip dhcp pool 1_WIFI
 network 172.12.113.0 255.255.255.128
 default-router 172.12.113.1
 dns-server 172.12.113.130
 domain-name rcomp-21-22-dh-g4
ip dhcp pool 1_VoIP
 network 172.12.114.64 255.255.255.192
 default-router 172.12.114.65
 option 150 ip 172.12.114.65
 dns-server 172.12.113.130
 domain-name rcomp-21-22-dh-g4
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX1017JB07-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.1
 encapsulation dot1Q 331
 ip address 172.12.114.1 255.255.255.192
 ip access-group 100 in
!
interface FastEthernet1/0.2
 encapsulation dot1Q 332
 ip address 172.12.112.129 255.255.255.128
 ip access-group 101 in
!
interface FastEthernet1/0.3
 encapsulation dot1Q 333
 ip address 172.12.113.1 255.255.255.128
 ip access-group 102 in
!
interface FastEthernet1/0.4
 encapsulation dot1Q 334
 ip address 172.12.113.129 255.255.255.128
!
interface FastEthernet1/0.5
 encapsulation dot1Q 335
 ip address 172.12.114.65 255.255.255.192
 ip access-group 103 in
!
interface FastEthernet1/0.6
 encapsulation dot1Q 330
 ip address 172.12.112.2 255.255.255.128
!
interface Vlan1
 no ip address
 shutdown
!
router ospf 5
 log-adjacency-changes
 network 172.12.114.0 0.0.0.63 area 1
 network 172.12.112.128 0.0.0.127 area 1
 network 172.12.113.0 0.0.0.127 area 1
 network 172.12.113.128 0.0.0.127 area 1
 network 172.12.114.64 0.0.0.63 area 1
 network 172.12.112.0 0.0.0.127 area 0
!
router rip
!
ip nat inside source static tcp 172.12.113.131 80 172.12.112.2 80 
ip nat inside source static tcp 172.12.113.131 443 172.12.112.2 443 
ip nat inside source static tcp 172.12.113.130 53 172.12.112.2 53 
ip nat inside source static udp 172.12.113.130 53 172.12.112.2 53 
ip classless
!
ip flow-export version 9
!
!
access-list 102 permit ospf any any
access-list 102 permit udp any any eq bootps
access-list 102 permit udp any any eq bootpc
access-list 102 permit tcp 172.12.113.0 0.0.0.127 host 172.12.113.131 eq www
access-list 102 permit tcp 172.12.113.0 0.0.0.127 host 172.12.113.131 eq 443
access-list 102 deny tcp any 172.12.113.128 0.0.0.15 eq www
access-list 102 permit tcp 172.12.113.0 0.0.0.127 any eq www
access-list 102 deny tcp any 172.12.113.128 0.0.0.15 eq 443
access-list 102 permit tcp 172.12.113.0 0.0.0.127 any eq 443
access-list 102 permit udp 172.12.113.0 0.0.0.127 host 172.12.113.130 eq domain
access-list 102 permit icmp 172.12.113.0 0.0.0.127 any echo
access-list 102 permit icmp 172.12.113.0 0.0.0.127 any echo-reply
access-list 102 deny ip any host 172.12.113.1
access-list 100 permit ospf any any
access-list 100 permit udp any any eq bootps
access-list 100 permit udp any any eq bootpc
access-list 100 permit tcp 172.12.114.0 0.0.0.63 host 172.12.113.131 eq www
access-list 100 permit tcp 172.12.114.0 0.0.0.63 host 172.12.113.131 eq 443
access-list 100 deny tcp any 172.12.113.128 0.0.0.15 eq www
access-list 100 permit tcp 172.12.114.0 0.0.0.63 any eq www
access-list 100 deny tcp any 172.12.113.128 0.0.0.15 eq 443
access-list 100 permit tcp 172.12.114.0 0.0.0.63 any eq 443
access-list 100 permit udp 172.12.114.0 0.0.0.63 host 172.12.113.130 eq domain
access-list 100 permit icmp 172.12.114.0 0.0.0.63 any echo
access-list 100 permit icmp 172.12.114.0 0.0.0.63 any echo-reply
access-list 100 deny ip any host 172.12.114.1
access-list 101 permit ospf any any
access-list 101 permit udp any any eq bootps
access-list 101 permit udp any any eq bootpc
access-list 101 permit tcp 172.12.112.128 0.0.0.127 host 172.12.113.131 eq www
access-list 101 permit tcp 172.12.112.128 0.0.0.127 host 172.12.113.131 eq 443
access-list 101 deny tcp any 172.12.113.128 0.0.0.15 eq www
access-list 101 permit tcp 172.12.112.128 0.0.0.127 any eq www
access-list 101 deny tcp any 172.12.113.128 0.0.0.15 eq 443
access-list 101 permit tcp 172.12.112.128 0.0.0.127 any eq 443
access-list 101 permit udp 172.12.112.128 0.0.0.127 host 172.12.113.130 eq domain
access-list 101 permit icmp 172.12.112.128 0.0.0.127 any echo
access-list 101 permit icmp 172.12.112.128 0.0.0.127 any echo-reply
access-list 101 deny ip any host 172.12.112.129
access-list 103 permit ospf any any
access-list 103 permit udp any any eq bootps
access-list 103 permit udp any any eq bootpc
access-list 103 permit udp 172.12.114.64 0.0.0.63 host 172.12.114.65 eq tftp
access-list 103 permit tcp 172.12.114.64 0.0.0.63 host 172.12.114.65 eq 2000
access-list 103 permit ip 172.12.114.64 0.0.0.63 any
!
!
!
!
!
!
dial-peer voice 1 voip
 destination-pattern 2...
 session target ipv4:172.12.115.113
!
dial-peer voice 2 voip
 destination-pattern 3...
 session target ipv4:172.12.116.97
!
dial-peer voice 3 voip
 destination-pattern 4...
 session target ipv4:172.12.117.97
!
telephony-service
 max-ephones 2
 max-dn 2
 ip source-address 172.12.114.65 port 2000
 auto assign 1 to 2
!
ephone-dn 1
 number 1000
!
ephone-dn 2
 number 1001
!
ephone 1
 device-security-mode none
 mac-address 00D0.9789.5051
 type 7960
 button 1:1
!
ephone 2
 device-security-mode none
 mac-address 0040.0BAA.7577
 type 7960
 button 1:2
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

