RCOMP 2021-2022 Project - Sprint 3 - Member 1200735 folder
===========================================

# RCOMP 2021-2022 Sprint 3

O presente documento pretende expor como ocorreu a alteração de routing tables estáticas para dinâmicas com o OSPF, a implementação do servidor HTTP, do serviço DHCP e VoIP , o estabelecimento de dominios DNS e finalmente a implementação do NAT(Network Address Translation) e das firewall estáticas(ACLs).

## Building 2 ##


## 1. OSPF

De modo a garantir uma correta simulação, o OSPF foi configurado em cada router do edifício, respeitando as áreas definidas no planning.
Para a configuração foram utilizados os ip's referentes a cada edifício:

### Router MC:

    router ospf 5
    network 172.12.112.1 0.0.0.127 area 0
    default-information originate


### Edifício 1:

    router ospf 5 
    network 172.12.114.1 0.0.0.63 area 1 
    network 172.12.112.129 0.0.0.127 area 1 
    network 172.12.113.1 0.0.0.127 area 1 
    network 172.12.113.129 0.0.0.127 area 1 
    network 172.12.114.65 0.0.0.63 area 1 
    network 172.12.112.2 0.0.0.127 area 0


### Edifício 2:

    router ospf 5 
    network 172.12.115.65 0.0.0.31 area 2 
    network 172.12.115.1 0.0.0.63 area 2 
    network 172.12.114.129 0.0.0.127 area 2 
    network 172.12.115.97 0.0.0.15 area 2 
    network 172.12.115.113 0.0.0.15 area 2 
    network 172.12.112.3 0.0.0.127 area 0

### Edifício 3:

    router ospf 5 
    network 172.12.115.129 0.0.0.63 area 3 
    network 172.12.115.193 0.0.0.63 area 3 
    network 172.12.116.1 0.0.0.63 area 3 
    network 172.12.116.65 0.0.0.31 area 3 
    network 172.12.116.97 0.0.0.31 area 3 
    network 172.12.112.4 0.0.0.127 area 0

### Edifício 4:

    router ospf 5 
    network 172.12.117.65 0.0.0.31 area 4 
    network 172.12.117.1 0.0.0.63 area 4 
    network 172.12.116.129 0.0.0.127 area 4 
    network 172.12.117.113 0.0.0.15 area 4 
    network 172.12.117.97 0.0.0.15 area 4 
    network 172.12.112.5 0.0.0.127 area 0

## 2. Servidor HTTP

Foi adicionada um servidor em cada rede DMZ e foi-lhe definido um ip: 172.12.115.99, depois foi ativado o serviço HTTP e criada uma página HTML.

![html_page.png](imagens/html_page.png)

## 3. DHCP

No router foi configurado o serviço DHCP para VoIP, WiFi e para os dois end users (piso 0 e piso 1). O nome de cada dhcp está definido no comando "ip dhcp pool xxx", em que xxx corresponde ao nome do respetivo dhcp sendo este o mesmo nome da VLAN.
A cada dhcp, há um endereço ipv4 correspondente. Se se analisar o sprint anterior, repara-se que a cada VLAN existe um default gateway, e este endereço corresponde à network do dhcp.
É de destacar também que existe um nome do domain, "building-2.rcomp-21-22-dh-g4", tal como definido no planning.
Em todos os dhcp existe um default router correspondente.

### Floor 0:

    ip dhcp pool 2_EndUser_0 
    network 172.12.115.64 255.255.255.224 
    default-router 172.12.115.65 
    domain-name building-2.rcomp-21-22-dh-g4 
    dns-server 172.12.115.98 (ip do server dmz)


### Floor 1:

    ip dhcp pool 2_EndUser_1 
    network 172.12.115.0 255.255.255.192 
    default-router 172.12.115.1 
    domain-name building-2.rcomp-21-22-dh-g4 
    dns-server 172.12.115.98


### WiFi:

    ip dhcp pool 2_WIFI 
    network 172.12.114.128 255.255.255.128 
    default-router 172.12.114.129 
    domain-name building-2.rcomp-21-22-dh-g4 
    dns-server 172.12.115.98


### VoIP:

    ip dhcp pool 2_VoIP 
    network 172.12.115.112 255.255.255.240 
    default-router 172.12.115.113 
    domain-name building-2.rcomp-21-22-dh-g4 
    dns-server 172.12.115.98
    option 150 ip 172.12.115.113

    ip dhcp excluded-address 172.12.115.65 
    ip dhcp excluded-address 172.12.115.1 
    ip dhcp excluded-address 172.12.114.129 
    ip dhcp excluded-address 172.12.115.113

## VoIP

Para a configuração dos telefones, é necessário desde logo correr os seguintes comandos nas interfaces ligadas aos mesmos.

    switchport mode access 
    switchport voice vlan 172.12.115.113 
    no switchport access vlan

Depois, no router, correram-se os seguintes comandos:

    telephony-service 
    no auto-reg-ephone 
    ip source-address 172.12.115.113 port 2000 (Gateway VoIP do edificio 2) 
    max-ephones 20 
    max-dn 20 
    exit

Aqui são criados os números de telefone, tal como definidos no planning.

    ephone-dn 1 
    number 2001 
    exit 
    ephone-dn 2 
    number 2002 
    exit

Aqui, os números criados são associados manualmente aos telefones.

    ephone 1 
    type 7960  
    mac-address 0090.0CA6.E495 (MAC Address associado ao telefone) 
    button 1:1 
    exit 

    ephone 2 
    type 7960  
    mac-address 0009.7C20.92C8 
    button 1:2 
    exit

Por último, é configurado o reencaminhamento de chamadas para cada um dos restantes edifícios.

    dial-peer voice 1 voip 
    destination-pattern 1... 
    session target ipv4:172.12.114.65 (Gateway VoIP edificio 1) 
    exit 

    dial-peer voice 3 voip 
    destination-pattern 3... 
    session target ipv4:172.12.116.97
    exit 

    dial-peer voice 4 voip 
    destination-pattern 4... 
    session target ipv4:172.12.117.97 
    end

## DNS

Para configurar o dominio DNS no edifício 2 foi necessário no servidor DNS criar a seguinte tabela:

![dnsTable.png](imagens/dnsTable.png)


## NAT(Network Address Translation)

De modo a redirecionar os pedidos de HTTP e HTTPS para o servidor HTTP no DMZ local no router foi feita a seguinte configuração utilizando as portas 80 e 443 como default.

    ip nat inside source static tcp 172.12.115.99 80 172.12.112.3 80 
    ip nat inside source static tcp 172.12.115.99 443 172.12.112.3 443

Todos os pedidos DNS recebido são também redirecionados do router da backbone para o servidor local de DNS sendo o número da porta default a 53.

    ip nat inside source static tcp 172.12.115.98 53 172.12.112.3 53 
    ip nat inside source static udp 172.12.115.98 53 172.12.112.3 53

A configuração apresentada irá ser aplicada a pacotes provenientes do servidor DNS e servidor HTTP/HTTPS existentes na rede DMZ. De modo a ocultar o ip address a redes externas do edifício, os pacotes que tenham como destino uma dessas, terão o seu ip alterado no router, para o ip do router para a VLAN do Backbone. O oposto irá ocorrer.
Com isto, um end node que tente aceder ao servidor HTTP, terá de ir pelo endereço do router do edifício 1 para a VLAN do backbone.


<br />
<br />

![NATtable.png](imagens/NATtable.png)

## Static Firewalls (ACLs)

Era pedido:

* bloqueio do Internal Spoofing;
* permissão todos os ICMP echo requests e echo replies;
* permissão todo o trafico do DMZ, bloquear todo o tráfego direcionado ao router, exceto o tráfego necessário para os recursos atuais funcionarem (regras estáticas DHCP, TFTP, ITS, OSPF, NAT);

#### VLAN WI-FI

    access-list 102 permit ospf any any
    access-list 102 permit udp any any eq 67
    access-list 102 permit udp any any eq 68
                                // IP Network VLAN Wifi     // IP HTTP 
    access-list 102 permit tcp 172.12.114.128 0.0.0.127 host 172.12.115.99 eq 80
    access-list 102 permit tcp 172.12.114.128 0.0.0.127 host 172.12.115.99 eq 443
                                 // IP Network VLAN DMZ
    access-list 102 deny tcp any 172.12.115.96 0.0.0.15 eq 80
                                 // IP Network VLAN Wifi
    access-list 102 permit tcp 172.12.114.128 0.0.0.127 any eq 80
                                 // IP Network VLAN DMZ
    access-list 102 deny tcp any 172.12.115.96 0.0.0.15 eq 443
                                 // IP Network VLAN Wifi
    access-list 102 permit tcp 172.12.114.128 0.0.0.127 any eq 443
                                                        // IP Server DNS
    access-list 102 permit udp 172.12.114.128 0.0.0.127 host 172.12.115.98 eq 53
    access-list 102 permit tcp 172.12.114.128 0.0.0.127 host 172.12.115.98 eq 53
    access-list 102 permit icmp 172.12.114.128 0.0.0.127 any echo
    access-list 102 permit icmp 172.12.114.128 0.0.0.127 any echo-reply
                                // IP Gateway VLAN DMZ
    access-list 102 deny ip any host 172.12.115.97


#### VLAN END USERS PISO 0

    access-list 100 permit ospf any any
    access-list 100 permit udp any any eq 67
    access-list 100 permit udp any any eq 68
                             // IP Network VLAN Piso 0   // IP Server HTTP
    access-list 100 permit tcp 172.12.115.64 0.0.0.31 host 172.12.115.99 eq 80
    access-list 100 permit tcp 172.12.115.64 0.0.0.31 host 172.12.115.99 eq 443
                             // IP Network VLAN DMZ
    access-list 100 deny tcp any 172.12.115.96 0.0.0.15 eq 80
                             // IP Network VLAN Piso 0
    access-list 100 permit tcp 172.12.115.64 0.0.0.31 any eq 80
                             // IP Network VLAN DMZ
    access-list 100 deny tcp any 172.12.115.96 0.0.0.15 eq 443
                             // IP Network VLAN Piso 0
    access-list 100 permit tcp 172.12.115.64 0.0.0.31 any eq 443
                                                        // IP Server DNS
    access-list 100 permit udp 172.12.115.64 0.0.0.31 host 172.12.115.98 eq 53
    access-list 100 permit tcp 172.12.115.64 0.0.0.31 host 172.12.115.98 eq 53
    access-list 100 permit icmp 172.12.115.64 0.0.0.31 any echo
    access-list 100 permit icmp 172.12.115.64 0.0.0.31 any echo-reply
                                // IP Default Gateway Piso 0
    access-list 100 deny ip any host 172.12.115.65

#### VLAN END USERS PISO 1

    access-list 101 permit ospf any any
    access-list 101 permit udp any any eq 67
    access-list 101 permit udp any any eq 68
                             // IP Network VLAN Piso 1   // IP Server HTTP
    access-list 101 permit tcp 172.12.115.0 0.0.0.63 host 172.12.115.99 eq 80
    access-list 101 permit tcp 172.12.115.0 0.0.0.63 host 172.12.115.99 eq 443
                             // IP Network VLAN DMZ
    access-list 101 deny tcp any 172.12.115.96 0.0.0.15 eq 80
                             // IP Network VLAN Piso 1
    access-list 101 permit tcp 172.12.115.0 0.0.0.63 any eq 80
                             // IP Network VLAN DMZ
    access-list 101 deny tcp any 172.12.115.96 0.0.0.15 eq 443
                             // IP Network VLAN Piso 1
    access-list 101 permit tcp 172.12.115.0 0.0.0.63 any eq 443
                                                        // IP Server DNS
    access-list 101 permit udp 172.12.115.0 0.0.0.63 host 172.12.115.98 eq 53
    access-list 101 permit tcp 172.12.115.0 0.0.0.63 host 172.12.115.98 eq 53
    access-list 101 permit icmp 172.12.115.0 0.0.0.63 any echo
    access-list 101 permit icmp 172.12.115.0 0.0.0.63 any echo-reply
                                // IP Default Gateway Piso 1
    access-list 101 deny ip any host 172.12.115.1


#### VLAN VoIP

    access-list 103 permit ospf any any
    access-list 103 permit udp any any eq 67
    access-list 103 permit udp any any eq 68
                 // IP Network VLAN VoIP // IP Default Gateway VoIP
    access-list 103 permit udp 172.12.115.112 0.0.0.15 host 172.12.115.113 eq 69
    access-list 103 permit tcp 172.12.115.112 0.0.0.15 host 172.12.115.113 eq 2000
    access-list 103 permit ip 172.12.115.112 0.0.0.15 any

#### VLAN BACKBONE


    // IP das VLANS do edifício 
    access-list 104 deny ip 172.12.115.64 0.0.0.31 any
    access-list 104 deny ip 172.12.115.0 0.0.0.63 any
    access-list 104 deny ip 172.12.114.128 0.0.0.127 any
    access-list 104 deny ip 172.12.115.96 0.0.0.15 any
    access-list 104 deny ip 172.12.115.112 0.0.0.15 any
                                    // IP Backbone associado ao edifício
    access-list 104 permit udp any host 172.12.112.3 eq 53
    3. access-list 104 permit tcp any host 172.12.112.3 eq 53
    4. access-list 104 permit tcp any host 172.12.112.3 eq 80
    5. access-list 104 permit tcp any host 172.12.112.3 eq 443
    6. access-list 104 permit tcp any host 172.12.112.3 eq 2000
    8. access-list 104 permit ospf any any
    9. access-list 104 permit icmp any any echo
    10. access-list 104 permit icmp any any echo-reply
    11. access-list 104 deny ip any host 172.12.112.3
    12. access-list 104 permit ip any any


### Associar ACL

#### Piso 0:

    interface FastEthernet1/0.1
    ip access-group 100 in

#### Piso 1:

    interface FastEthernet1/0.2
    ip access-group 101 in

#### Wifi:

    interface FastEthernet1/0.3
    ip access-group 102 in


#### VoIP:

    interface FastEthernet1/0.5
    ip access-group 103 in

#### Backbone:

A ACL relativa ao backbone não foi implementada devido a problemas. Quando a adicionávamos, não conseguiamos fazer chamadas, apenas receber. Não conseguimos fazer troubleshooting do problema.

