RCOMP 2021-2022 Project - Sprint 3 planning
===========================================
### Sprint master: 1191706 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
all the members of Sprint will configure the layer three of the building that was assigned to you in the previous sprint.
T.3.1 Update the building1.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 1.
T.3.2 Update the building2.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 2.
Final integration of each member’s Packet Tracer simulation into asingle simulation (campus.pkt).
T.3.3 Update the building3.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 3.
T.3.4 Update the building4.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 4.



# 2. Technical decisions and coordination #
In this section, all technical decisions taken in the planning meeting should be mentioned. 		Most importantly, all technical decisions impacting on the subtasks implementation must be settled on this 		meeting and specified here.

# 2.1 VoIP #

| Building | Phone's Number Prefix  |
|:---------------------------:|:---------|
|         Building1           | 1xxx     |
|         Building2           | 2xxx     |
|         Building3           | 3xxx     |
|         Building4           | 4xxx     |

# 2.2 OSPF #

| Network | Area ID  |
|:---------------------------:|:---------|
|         Backbone            |  0    |
|         Building1           | 1     |
|         Building2           | 2     |
|         Building3           | 3     |
|         Building4           | 4     |

# 2.3 DNS #

Domain: rcomp-21-22-dh-g4

| Building | Local Domain  |
|:---------------------------:|:---------|
|         Building1           | building-1.rcomp-21-22-dh-g4     |
|         Building2           | building-2.rcomp-21-22-dh-g4     |
|         Building3           | building-3.rcomp-21-22-dh-g4     |
|         Building4           | building-4.rcomp-21-22-dh-g4     |

| Building | DNS IP  |
|:---------------------------:|:---------|
|         Building1           | 172.12.113.130     |
|         Building2           | 172.12.115.98     |
|         Building3           | 172.12.116.66     |
|         Building4           | 172.12.117.115     |


# 3. Subtasks assignment #

  * 1201623 - Building 1
  * 1191706 - Building 4
  * 1191568 - Building 3
  * 1200735 - Building 2
