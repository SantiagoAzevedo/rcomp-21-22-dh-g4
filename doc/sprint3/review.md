RCOMP 2021-2022 Project - Sprint 3 review
=========================================
### Sprint master: 1191706 ###
# 1. Sprint's backlog #
* **T.3.1** Update the building1.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 1.
* **T.3.2** Update the building2.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 2. Final integration of each member’s Packet Tracer simulation into a single simulation (campus.pkt).
* **T.3.3** Update the building3.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 3.
* **T.3.4** Update the building4.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 4.

# 2. Subtasks assessment #

## 2.1. 1201623 - Layer three update for building 1 #
### This sprint was mostly completed with the exception of the ACL implementation for the backbone. ###
## 2.2. 1200735 - Layer three update for building 2 #
### This sprint was mostly completed with the exception of the ACL implementation for the backbone. ###
## 2.3. 1191568 - Layer three update for building 3 #
### This sprint was mostly completed with the exception of the ACL implementation for the backbone. ###
## 2.4. 1191706 - Layer three update for building 4 #
### This sprint was mostly completed with the exception of the ACL implementation for the backbone. ###


