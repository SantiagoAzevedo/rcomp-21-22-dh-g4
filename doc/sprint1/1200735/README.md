RCOMP 2021-2022 Project - Sprint 1 - Member 1200735 folder
===========================================

# Building 2 #

The horizontal dimensions of building 2 are approximately 20 x 20 meters.

## Floor 0 Plant ##

![Building2_Floor0_Plant](Building2_Floor0_Plant.png)

The ground floor has an underfloor cable raceway connected to the external technical ditch. Access to the underfloor cable raceway is available at points marked over the plan. The ceiling height on this floor is 4 meters.

## Floor 1 Plant ##

![Building2_Floor1_Plant](Building2_Floor1_Plant.png)

This floor has no underfloor cable raceway. The ceiling height on this floor is 3 meters, however there’s a removable dropped ceiling, placed 2.5 meters from the ground, covering the entire floor. The space over the dropped ceiling is perfect to install cable raceways.

# Outlets #

To calculate the needed ammount of outlets for each room, we need to measure the rooms. Outlets were kept as much as possible on the wall, except for situations where it was inevitable (2.0.2 and 2.0.3). Also, we needed to guarantee that for each 10 m<sup>2</sup>  we'd have 2 outlets.

## Floor 0 ##

### Room 2.0.1 ###

**Top:** 3 m 

**Side:** 3.56 m 

Area is approximately 10.68 m<sup>2</sup> 

### Room 2.0.2 ###

**Top:** 5.78 m

**Side:** 5.2 m

Area is approximately 30.1 m<sup>2</sup>

**Outlets:** 6

### Room 2.0.3 ###

**Top:** 9.9 m

**Side:** 8.5 m

Area is approximately 84.15 m<sup>2</sup>

**Outlets:** 16 

### Room 2.0.4 ###

**Top:** 8.5 m

**Side:** 3,3 m

Area is approximately 28.1 m<sup>2</sup>

**Outlets:** 6

### Access point ###

The access point outlet was put in the ceiling on an open space near the stairs, to be as centered as possible. The cabling to it is done through room 2.0.2, with a small hole on the wall, near the ceiling, for the cable to connect there.

## Floor 1 ##

### Room 2.1.1 ###

**Top:** 3.56 m

**Side:** 3.56 m

Area is approximately 10.75 m<sup>2</sup>

No outlets required.

### Room 2.1.2 ###

**Top:** 3 m

**Side:** 5.2 m

Area is approximately 15.6 m<sup>2</sup>

**Outlets:** 4

### Room 2.1.3 ###

**Top:** 3 m

**Side:** 5.2 m

Area is approximately 15.6 m<sup>2</sup>

**Outlets:** 4 

### Room 2.1.4 ###

**Top:** 2.75 m

**Side:** 5.2 m

Area is approximately 14.3 m<sup>2</sup>

**Outlets:** 4 

### Room 2.1.5 ###

**Top:** 2.75 m

**Side:** 5.2 m

Area is approximately 14.3 m<sup>2</sup>

**Outlets:** 4 

### Room 2.1.6 ###

**Top:** 3.3 m

**Side:** 5.2 m

Area is approximately 17.2 m<sup>2</sup>

**Outlets:** 4 

### Room 2.1.7 ###

**Top:** 6.3 m

**Side:** 3.3 m

Area is approximately 20.8 m<sup>2</sup>

**Outlets:** 6 

### Room 2.1.8 ###

**Top:** 6.3 m

**Side:** 3.3 m

Area is approximately 20.8 m<sup>2</sup>

**Outlets:** 6 

### Room 2.1.9 ###

**Top:** 3 m

**Side:** 5.2 m

Area is approximately 15.7 m<sup>2</sup>

**Outlets:** 4 

### Room 2.1.10 ###

**Top:** 3 m

**Side:** 5.2 m

Area is approximately 15.7 m<sup>2</sup>

**Outlets:** 4 

### Room 2.1.11 ###

**Top:** 3 m

**Side:** 5.2 m

Area is approximately 15.7 m<sup>2</sup>

**Outlets:** 4 

### Room 2.1.12 ###

**Top:** 3.6 m

**Side:** 5.2 m

Area is approximately 18.6 m<sup>2</sup>

**Outlets:** 4 

### Access point ###

The access point outlet was also put in the ceiling on an open space near the stairs, to be as centered as possible. Since we have a dropped ceiling, the cable that connects to it goes directly through a cable raceway above.

# Cross Connects #

## Floor 0 ##

Room 2.0.1 is used as a telecommunications room, where an Intermediate Cross Connect (IC) and an Horizontal Cross Connect (HC) are housed, 2 meters above the ground.
Also, because room 2.0.3 requires 16 outlets, a Consolidation Point was put there, to minimize the length of necessary cables.

### IC ###

Incoming cables: 16 (8 from MC + 8 from IC from Building 1)

Outgoing cables: 24 (8 for HC in floor 0 + 8 for HC in floor 1 + 8 for next building for redundancy)

### HC ###

Incoming cables: 8

Outgoing cables: 25 + 8 fiber cables to floor 1 HC

### CP ###

Incoming cables: 4

Outgoing cables: 16

## Floor 1 ##

Room 2.1.1 was used as a telecommunications room, where a Horizontal Cross Connect (HC) is housed, 1.5 meters above the ground.

### HC ###

Incoming cables: 16 (8 from IC and 8 from HC)

Outgoing cables: 57 (56 WA outlets + 1 access point outlet)

# Cables #

As defined for the whole project, CAT7 cables were used for the Horizontal Cabling and Monomode fiber for the Backbone Cabling. 

Monomode fiber was chosen as it is immune to dispersion and thus allowing higher data rates and especially longer cable lengths, several thousands of meters. 

CAT7 was chosen for data rates of 10 Gbps.

## Floor 0 ##

### Backbone cabling (monomode fiber) ###

* 0.9 meters (from entrance of underfloor cable raceway to floor cable passageway in room 2.0.1) 
* 1.15 meters (from floor cable passageway to IC zone (horizontally))
* 2 meters (from the floor to IC)
* 0.2 meters (IC to HC)
* 2 meters (cables for floor above - from IC to the floor)
* 1.65 meters (from floor near IC to floor near ceiling passageway)
* 4 meters (from floor to ceiling cable passageway)
* 

<br /> 

**Total** = 11.9 * 8 + 7.35 (connection between HC) * 8 = 154 meters

160 meters of monomode fiber cables will be requested, with tolerance.

### Horizontal cabling (CAT7) ###

* 2 meters (from HC to underfloor cable raceway)
* 3.15 meters (from underfloor cable raceway entrance in 2.0.1 to entrance in 2.0.2)
* 10 meters (from underfloor cable raceway entrance in 2.0.1 to Consolidation Point in 2.0.3)
* 27.6 meters (from underfloor cable raceway entrance in 2.0.1 to entrance in 2.0.4)
* 34.2 meters (from underfloor cable raceway entrance in 2.0.1 to entrance in 2.0.5)
* 37.85 meters (from underfloor cable raceway entrance in 2.0.1 to entrance in 2.0.6)

#### Room 2.0.2 ####

Closest outlet: 1.7 m

Farthest outlet: 9.2 m 

Average = (1.7 + 9.2) / 2 = 5.45 m 

Cables inside room: 6 outlets * 5.45 = 32.7 m

Cables until room: (2 + 3.15) * 6 = 30.9 m

#### Room 2.0.3 ####

Closest outlet: 1 m

Farthest outlet: 25.4 m 

Average = (1 + 25.4) / 2 = 13.2 m 

Cables inside room: 16 outlets * 13.185 = 210.96 m

Cables until room: 10 m * 4 (for redundancy) = 40 m

#### Room 2.0.4 ####

Closest outlet: 1.6 m

Farthest outlet: 10 m 

Average = (1.6 + 10) / 2 = 5.8 m 

Cables inside room: 6 outlets * 5.8 = 34.8 m

Cables until room: (2 + 27.6) * 6 = 177.6 m

#### Room 2.0.5 ####

Closest outlet: 1.6 m

Farthest outlet: 10 m 

Average = (1.6 + 10) / 2 = 5.8 m 

Cables inside room: 6 outlets * 5.8 = 34.8 m

Cables until room: (2 + 34.2) * 6 = 217.2 m

#### Room 2.0.6 ####

Closest outlet: 1.6 m

Farthest outlet: 10 m 

Average = (1.6 + 10) / 2 = 5.8 m 

Cables inside room: 6 outlets * 5.8 = 34.8 m

Cables until room: (2 + 37.85) * 6 = 239.1 m

#### Access Point ####

2 + 3.15 + 10.15 = 15.3 m

<br /> 

**Total** = 32.7 + 30.9 + 210.96 + 40 + 34.8 + 177.7 + 34.8 + 217.2 + 34.8 + 239.1 + 15.3 = 1068.26 meters

1300 meters of CAT7 cables will be requested, with tolerance.

## Floor 1 ##

### Backbone cabling (monomode fiber) ###

* 1.2 meters (from floor cable passageway to floor near HC)
* 1.5 meters (from floor to HC)

<br /> 

**Total** = 2.7 * 16 (8 from IC + 8 from HC) = 43.2 meters

50 meters of monomode fiber cables will be requested.

### Horizontal cabling (CAT7) ###

* 1.9 meters (from HC to ceiling cable passageway)
* 7 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.2)
* 10.35 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.3)
* 13.4 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.4)
* 16.5 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.5)
* 20 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.6)
* 35.15 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.7)
* 28.3 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.8)
* 48.1 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.9)
* 41.9 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.10)
* 41.3 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.11)
* 34.5 meters (from ceiling cable passageway entrance in 2.1.1 to near-floor cable raceway in 2.1.12)

#### Room 2.1.2 ####

Closest outlet: 1 m

Farthest outlet: 6.3 m 

Average = (1 + 6.3) / 2 = 3.65 m 

Cables inside room: 4 outlets * 3.65 = 14.6 m

Cables until room: (1.9 + 7) * 4 = 35.6 m


#### Room 2.1.3 ####

Closest outlet: 1 m

Farthest outlet: 6.3 m 

Average = (1 + 6.3) / 2 = 3.65 m 

Cables inside room: 4 outlets * 3.65 = 14.6 m

Cables until room: (1.9 + 10.35) * 4 = 49 m


#### Room 2.1.4 ####

Closest outlet: 1 m

Farthest outlet: 6.1 m 

Average = (1 + 6.1) / 2 = 3.55 m 

Cables inside room: 4 outlets * 3.55 = 14.2 m

Cables until room: (1.9 + 13.4) * 4 = 61.2 m


#### Room 2.1.5 ####

Closest outlet: 1 m

Farthest outlet: 6.1 m 

Average = (1 + 6.1) / 2 = 3.55 m 

Cables inside room: 4 outlets * 3.55 = 14.2 m

Cables until room: (1.9 + 16.5) * 4 = 73.6 m


#### Room 2.1.6 ####

Closest outlet: 1 m

Farthest outlet: 6.75 m 

Average = (1 + 6.75) / 2 = 3.9 m 

Cables inside room: 4 outlets * 3.875 = 15.5 m

Cables until room: (1.9 + 20) * 4 = 87.6 m


#### Room 2.1.7 ####

Closest outlet: 1.5 m

Farthest outlet: 7.9 m 

Average = (1.5 + 7.9) / 2 = 4.7 m 

Cables inside room: 6 outlets * 4.7 = 28.2 m

Cables until room: (1.9 + 35.15) * 6 = 222.3 m 


#### Room 2.1.8 ####

Closest outlet: 1.5 m

Farthest outlet: 7.9 m 

Average = (1.5 + 7.9) / 2 = 4.7 m 

Cables inside room: 6 outlets * 4.7 = 28.2 m

Cables until room: (1.9 + 28.3) * 6 = 181.2 m


#### Room 2.1.9 ####

Closest outlet: 1.4 m

Farthest outlet: 6.75 m 

Average = (1.4 + 6.75) / 2 = 4.1 m 

Cables inside room: 4 outlets * 4.1 = 16.4 m

Cables until room: (1.9 + 48.1) * 4 = 200 m 


#### Room 2.1.10 ####

Closest outlet: 1.4 m

Farthest outlet: 6.75 m 

Average = (1.4 + 6.75) / 2 = 4.1 m 

Cables inside room: 4 outlets * 4.1 = 16.4 m

Cables until room: (1.9 + 41.9) * 4 = 175.2 m 


#### Room 2.1.11 ####

Closest outlet: 1.4 m

Farthest outlet: 6.75 m 

Average = (1.4 + 6.75) / 2 = 4.1 m 

Cables inside room: 4 outlets * 4.1 = 16.4 m

Cables until room: (1.9 + 41.3) * 4 = 172.8 m 


#### Room 2.1.12 ####

Closest outlet: 1.4 m

Farthest outlet: 7.9 m 

Average = (1.4 + 7.9) / 2 = 4.4 m 

Cables inside room: 4 outlets * 4.4 m = 17.6 m

Cables until room: (1.9 + 34.5) * 4 = 145.6 m

#### Access Point ####

1.9 + 12.95 = 14.85 m

<br /> 


**Total** = 14.6 + 35.6 + 14.6 + 49 + 14.2 + 61.2 + 14.2 + 73.6 + 15.5 + 87.6 + 28.2 + 222.3 + 28.2 + 181.2 + 16.4 + 200 + 16.4 + 175.2 + 16.4 + 172.8 + 17.6 + 145.6 + 14.85 = 1615.25 m


1700 meters of CAT7 cables will be requested, with tolerance.

## Total ##

### Backbone cabling (monomode fiber) ###

A total of 210 meters of monomode fiber cables will be requested.

### Horizontal cabling (CAT7) ###

A total of 3000 meters of CAT7 cable will be requested.


# Enclosures #

## Floor 0 ##

### IC ###

A fiber patch panel of 48 ports is necessary (1U). Hence we need an enclosure rack of 6 * 2 = 12U.

### HC ###

A fiber patch panel of 24 ports (1U) and a CAT7 patch panel of 48 ports is necessary (2U). Hence we need an enclosure rack of 6 * 3U = 18U.

### CP ###

A CAT7 patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 6U.

## Floor 1 ##

### HC ###

A fiber patch panel of 24 ports (1U) and a CAT7 patch panel of 48 ports is necessary (2U). Hence we need an enclosure rack of 6 * 3U = 18U.

# Total Inventory for Building 2 #

* 210 meters of monomode fiber cables 
* 3000 meters of CAT7 cable 
* 90 outlets
* 2 Access Point devices
* 1 Intermediate Cross-Connect
* 2 Horizontal Cross-Connect
* 1 Consolidation Point
* 2 Enclosure Racks of 18U
* 1 Enclosure Rack of 12U
* 1 Enclosure Rack of 6U
* 2 Fiber 24 port Patch Panel
* 1 Fiber 48 port Patch Panel
* 2 CAT7 48 port Patch Panels
* 1 CAT7 24 port Patch Panel
* At least 90 patch cords (for outlets), excluding the ones needed for Cross-Connects
