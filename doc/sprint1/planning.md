RCOMP 2021-2022 Project - Sprint 1 planning
===========================================
### Sprint master: 1201623 ###

# 1. Sprint's backlog #
The structured cabling project is to embrace a private closed area with five buildings, each of these
buildings has two floors. These buildings are numbered as 1, 2, 3, 4, and 5.

* **T.1.1** Development of a structured cabling project for building 1, encompassing the campus backbone.
* **T.1.2** Development of a structured cabling project for building 2.
* **T.1.3** Development of a structured cabling project for building 3.
* **T.1.4** Development of a structured cabling project for building 4.

# 2. Technical decisions and coordination #
* For all backbone is used monomode fibre.
* For horizontal cabling (from TR to work area) ) will only be used CAT7 cables.
* An access point will be used for each floor, covering up to 50 meters.
* Cabling redundancy (8 cables).
* In terms of cable lengths, it should be less than 90 meters.
* To measure the cables: calculate the distance from the outlet farther and the closest to each room and make the average.

#### Example: ####
  * 1201623 - Building 1
  * 1191706 - Building 4
  * 1191568 - Building 3
  * 1200735 - Building 2
