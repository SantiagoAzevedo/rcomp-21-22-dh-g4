RCOMP 2021-2022 Project - Sprint 1 - Member 1191568 folder
===========================================

# Building 3 #

# 1. Plants #

The horizontal dimensions of building 3 are approximately 20 x 20 meters.

## 1.1 - Floor 0 Plant ##

![Building3_Floor0_Plant](Building3_Floor0_Plant.png)

The ground floor has an underfloor cable raceway connected to the external technical ditch. Access to the underfloor cable raceway is available at points marked over the plan. The ceiling height on this floor is 4 meters.

## 1.2 -Floor 1 Plant ##

![Building3_Floor1_Plant](Building3_Floor1_Plant.png)

This floor has no underfloor cable raceway. The ceiling height on this floor is 3 meters, however there’s a removable dropped ceiling, placed 2.5 meters from the ground, covering the entire floor. The space over the dropped ceiling is perfect to install cable raceways.

# 2. Measures

To calculate the number of outlets, it was followed the instruction "Structured cabling standards specify a minimum of two outlets per work area, and also a ratio of two outlets for each 10 square meters of area.".

## 2.1 Floor 0


To calculate the needed ammount of outlets for each room, we need to measure the rooms. Also, we needed to guarantee that for each 10 m<sup>2</sup> we'd have 2 outlets.

## Outlets  

### Room 3.0.1

**Top:** 5 m

**Side:** 3 m

**Outlets:** 6

Area is approximately 15 m<sup>2</sup>

### Room 3.0.2 ###

**Top:** 5 m

**Side:** 3 m

**Outlets:** 6

Area is approximately 15 m<sup>2</sup>

### Room 3.0.3 ###

**Top:** 5 m

**Side:** 3 m

**Outlets:** 6

Area is approximately 15 m<sup>2</sup>


### Room 3.0.4 ###

**Top:** 5 m

**Side:** 3,5 m

**Outlets:** 6

Area is approximately 17,5 m<sup>2</sup>


### Room 3.0.5 ###

**Top:** 8.5 m

**Side:** 3,3 m

Area is approximately 28.05 m<sup>2</sup>

**Outlets:** 6

### Room 3.0.6 ###

**Top:** 8.5 m

**Side:** 3,3 m

Area is approximately 28.05 m<sup>2</sup>

**Outlets:** 6

### Room 3.0.7 ###

**Top:** 8.5 m

**Side:** 3,3 m

Area is approximately 28.05 m<sup>2</sup>

**Outlets:** 6


### Room 3.0.8 ###

**Top:** 3,3 m

**Side:** 4,1 m

Area is approximately 13,53 m<sup>2</sup>

**Outlets:** 4

### Room 3.0.9 ###

**Top:** 3,3 m

**Side:** 2,5

Area is approximately 28.1 m<sup>2</sup>

**Outlets:** No need


### Access point ###

The access point outlet was put in the ceiling on In the middle of the hallway, to be as centered as possible.The cabling to it is done through room 3.0.5, with a small hole on the wall, near the ceiling, for the cable to connect there.

## 2.2 - Floor 1 ##

To calculate the needed ammount of outlets for each room, we need to measure the rooms. Also, we needed to guarantee that for each 10 m<sup>2</sup> we'd have 2 outlets.

### Room 3.1.1 ###

**Top:** 6.1 m

**Side:** 4.7 m

Area is approximately 28.67 m<sup>2</sup>

**Outlets:** 6

### Room 3.1.2 ###

**Top:** 6.3 m

**Side:** 4.7 m

Area is approximately 29.61 m<sup>2</sup>

**Outlets:** 6

### Room 3.1.3 ###

**Top:** 4.1 m

**Side:** 4.9 m

Area is approximately 20.09 m<sup>2</sup>

**Outlets:** 6

### Room 3.1.4 ###

**Top:** 4.1 m

**Side:** 4.7 m

Area is approximately 19.27 m<sup>2</sup>

**Outlets:** 4

### Room 3.1.5 ###

**Top:** 4.1 m

**Side:** 4.7 m

Area is approximately 19.27 m<sup>2</sup>

**Outlets:** 4

### Room 3.1.6 ###

**Top:** 4.1 m

**Side:** 4.7 m

Area is approximately 19.27 m<sup>2</sup>

**Outlets:** 4

### Room 3.1.7 ###

**Top:** 6.3 m

**Side:** 4.1 m

Area is approximately 25.83 m<sup>2</sup>

**Outlets:** 6

### Room 3.1.8 ###

**Top:** 6.3 m

**Side:** 4.1 m

Area is approximately 25.83 m<sup>2</sup>

**Top:** 6.1 m

**Side:** 4.1 m

Area is approximately 25.83 m<sup>2</sup>

**Outlets:** 6

### Room 3.1.10 ###

**Top:** 3.2 m

**Side:** 2.5 m

Area is approximately 8 m<sup>2</sup>

**Outlets:** Not required

### Access point ###

The access point outlet was also put in the ceiling on an open space near the stairs, to be as centered as possible. Since we have a dropped ceiling, the cable that connects to it goes directly through a cable raceway above.**

# 3. Cross Connects #

## 3.1 - Floor 0 ##

Room 3.0.9 is used as a telecommunications room, where an Intermediate Cross Connect (IC) and an Horizontal Cross Connect (HC) are housed, 2 meters above the ground.
As there is not a large concentration of outlkets on floor 0 of this building there is no need for Consolidation Points (CP).

### IC ###

Incoming cables: 16

Outgoing cables: 24

### HC ###

Incoming cables: 16

Outgoing cables: 38


## 3.2 - Floor 1 ##

Room 3.1.10 was used as a telecommunications room, where a Horizontal Cross Connect (HC) is housed, 1.5 meters above the ground.

# 4. Cables
As defined for the whole project, CAT7 cables were used for the Horizontal Cabling and Monomode fiber for the Backbone Cabling.Monomode fiber was chosen as it is immune to dispersion and thus allowing higher data rates and especially longer cable lengths, several thousands of meters. 
and CAT7 was chosen for data rates of 10 Gbps.
To calculate the lenght of the cables, it was followed the instructions "When a high number of cables share the same segment of a pathway, measure the segment pathway length and multiply by the number of cables." and "When a high number of cables irradiates from the same point, we can estimate the average cable length and multiply by the number of cables. This will be accurate if the cables length distribution is symmetrical. For instance, we can estimate the average cable length based on the two longest cables and two shortest cables.".

## 4.1 Floor 0

### Backbone cabling (monomode fiber) ###
    
* 31.86 meters (from entrance of underfloor cable raceway to floor cable passageway in room 3.0.9)
* 4.69 meters (from floor cable passageway to IC zone (horizontally))
* 2 meters (from the floor to IC)
* 1 meter (IC to HC)
* 2 meters (cables for floor above - from IC to the floor)
* 3.06 meters (from floor near IC to floor near ceiling passageway)
* 4 meters (from floor to ceiling cable passageway)

<br /> 

**Total** = 48.61 * 8 = **388.88** meters

**800** meters of monomode fiber cables will be requested for tolerance.

### Horizontal cabling (CAT7) ###

To best position the access point, one solution will be to create a connection out of room 3.0.5 as you can see in the image above, since placing the AP inside a room can impair the signal transmitted by it. To connect the outlets were respected the suggestions: avoid putting outlets in the middle of the rooms and mainly use the underfloor cable raceways.

* 2 meters (from HC to underfloor cable raceway)
* 40.12 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.1)
* 37.09 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.2)
* 34.92 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.3)
* 30.78 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.4)
* 21.94 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.5)
* 18.3 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.6)
* 21.08 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.7)
* 2.9 meters (from underfloor cable raceway entrance in 3.0.9 to entrance in 3.0.8)


### Room 3.0.1 ###

Closest outlet: 1.33 m

Farthest outlet: 5.24 m

Average = (1.33 + 5.24) / 2 = 3.29 m

Cables inside room: 4 outlets * 3.29 = 32.7 m

Cables until room: (2 + 40.12) * 4 = 168.48 m

### Room 3.0.2 ###

Closest outlet: 1.26 m

Farthest outlet: 4.79 m

Average = (1.26  + 4.79) / 2 = 3.02 m

Cables inside room: 4 outlets * 3.02 = 12.1 m

Cables until room: (2 + 37.09) * 4 = 156.36 m


### Room 3.0.3 ###

Closest outlet: 0.3 m

Farthest outlet: 5.68 m

Average = (0.3 + 5.68) / 2 = 2.99 m

Cables inside room: 4 outlets * 2.99 = 11.96 m

Cables until room: (2+11.96) m * 4 = 147.68 m

### Room 3.0.4 ###

Closest outlet: 0.9 m

Farthest outlet: 4.71 m

Average = (0.9+4.71) / 2 = 2.805 m

Cables inside room: 6 outlets * 2.805 = 16.83 m

Cables until room: (2 + 30.78) * 6 = 196.68 m

### Room 3.0.5 ###

Closest outlet: 0.43 m

Farthest outlet: 9.73 m

Average = (0.43 + 9.73) / 2 = 5.08 m

Cables inside room: 6 outlets * 5.8 = 30.48 m

Cables until room: (2 + 21.94) * 6 = 143.64 m

### Room 3.0.6 ###

Closest outlet: 1.52 m

Farthest outlet: 11.35 m

Average = (1.52 + 11.35) / 2 = 6.435 m

Cables inside room: 6 outlets * 6.435 = 38.61 m

Cables until room: (2 + 18.3) * 6 = 121.8 m

### Room 3.0.7 ###

Closest outlet: 1.52 m

Farthest outlet: 11.35 m

Average = (1.52 + 11.35) / 2 = 6.435 m

Cables inside room: 6 outlets * 6.435 = 38.61 m

Cables until room: (2 + 21.8) * 6 = 142.8 m

### Room 3.0.8 ###

Closest outlet: 1.14 m

Farthest outlet: 8.97 m

Average = (1.14 + 8.97) / 2 = 5.055 m

Cables inside room: 4 outlets * 5.055 = 20.22 m

Cables until room: (2 + 2.9) * 4 = 239.1 m


### Access Point ###

2 + 18.42 +3.46+8.38=32.26 m
<br /> 

**Total** = 13.14 + 168.48 + 12.1 + 156.36 + 11.96 + 147.68 + 16.83 + 196.68 + 30.48 + 143.64 + 38.61 + 121.8 + 38.61 + 142.8 +20.22 + 19.6 + 32.26 = 1311.25

1400 meters of CAT7 cables will be requested, with tolerance.




## 4.2 - Floor 1 ##

### Backbone cabling (monomode fiber) ###
* 
* 0.5 meters (from floor cable passageway to floor near HC)
* 1.5 meters (from floor to HC)
* 4.73 (from Hc on floor 0) + 4 (height on floor 0) + 1.5 + 0.5 = 10.73 m

<br /> 

**Total** = 12.73 * 8 = 16 meters

18 meters of monomode fiber cables will be requested.

### Horizontal cabling (CAT7) ###

Not to have many cables through the walls in the rooms and consequently obstruct the same, the outlets of the same working area enter the same place and descend almost to the floor to go through the rest of the room, as happened on the lower floor but in reverse. The different passageways of the cables were differentiated in the plant, with different colors to distinguish between the cables that pass through the removable dropped ceiling and the cables that go through the walls. As there is a removable dropped ceiling, the installation of the access point is facilitated, so it is enough to connect a cable to the HC. 

* 1.9 meters (from HC to ceiling cable passageway)
* 29.48 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.1)
* 23.62 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.2)
* 19.27  meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.3)
* 8.8 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.4)
* 14.16 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.5)
* 15.47 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.6)
* 17.91 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.7)
* 9.04 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.8)
* 2.57 meters (from ceiling cable passageway entrance in 3.1.10 to near-floor cable raceway in 3.1.9)

### Room 3.1.1 ###

Closest outlet: 0.25 m

Farthest outlet: 14.53 m

Average = (0.25 +14.53) / 2 = 7.39 m

Cables inside room: 6 outlets * 7.39 = 44.34 m

Cables until room: (1.9 + 29.48) * 6 = 188.28 m

### Room 3.1.2 ###

Closest outlet: 2.23 m

Farthest outlet: 17.63 m

Average = (2.23 + 17.63) / 2 = 9.93  m

Cables inside room: 6 outlets * 9.93 = 59.58 m

Cables until room: (1.9 + 23.62) * 6 = 153.12 m

### Room 3.1.3 ###

Closest outlet: 0.37 m

Farthest outlet: 12.46 m

Average = (0.37 + 12.46) / 2 = 6.415 m

Cables inside room: 6 outlets * 6.415 = 38.49 m

Cables until room: (1.9 + 19.27) * 4 = 127.02 m


### Room 3.1.4 ###

Closest outlet: 0.1 m

Farthest outlet: 11.64 m

Average = (0.1 + 11.64) / 2 = 5.87 m

Cables inside room: 4 outlets * 5.87 = 23.48 m

Cables until room: (1.9 + 8.8) * 4 = 42.8 m


### Room 3.1.5 ###

Closest outlet: 0.1 m

Farthest outlet: 11.9 m

Average = (0.1 + 11.9) / 2 = 6 m

Cables inside room: 4 outlets * 6 = 24 m

Cables until room: (1.9 + 14.16) * 4 = 64.24 m


### Room 3.1.6 ###

Closest outlet: 0.46 m

Farthest outlet: 12.2 m

Average = (0.46 + 12.2) / 2 = 6.33 m

Cables inside room: 4 outlets * 6.33 = 25.32 m

Cables until room: (1.9 + 15.47) * 4 = 69.48 m


### Room 3.1.7 ###

Closest outlet: 2.13 m

Farthest outlet: 17.59 m

Average = (2.13 + 17.69) / 2 =  9.86 m

Cables inside room: 6 outlets * 9.86 = 59.16 m

Cables until room: (1.9 + 17.91) * 6 = 118.86 m


### Room 3.1.8 ###

Closest outlet: 1.91 m

Farthest outlet: 15.71 m

Average = (1.91 + 15.71) / 2 = 8.81 m

Cables inside room: 6 outlets * 8.81 = 52.86 m

Cables until room: (1.9 + 9.04) * 6 = 65.64 m


### Room 3.1.9 ###

Closest outlet: 1.77 m

Farthest outlet: 12.29 m

Average = (1.77 + 12.29) / 2 = 7.03 m

Cables inside room: 6 outlets * 7.03 = 42.18 m

Cables until room: (1.9 + 2.57) * 6 = 26.82 m

### Access Point ###

0.70 + 0.38+ 8.55 + 2.46 = 12.09 m

<br /> 


**Total** = 44,34 + 188,28 + 59,58 + 153,12 + 38,49 +127,02 + 23,48 + 42.8 + 24 + 64.24 +25,32 +68.48 + 59,16+ 118.86 + 52,86+ 65.64 + 42,18 + 26.82 + 12.09 =1237.76

1300 meters of CAT7 cables will be requested, with tolerance.

## Total of building ##

### Backbone cabling (monomode fiber) ###

A total of **404.88 meters of monomode fiber** cables will be requested.

### Horizontal cabling (CAT7) ###

A total of **2 700 meters of CAT7** cable will be requested (without tolerance).


# 5. Enclosures #

## 5.1 - Floor 0 ##

### IC ###

A fiber patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 6U.

### HC ###

A CAT7 patch panel of 48 ports is necessary (2U). Hence we need an enclosure rack of 6 * 2 = 12U.

## 5.2 - Floor 1 ##

### HC ###

A CAT7 patch panel of 48 ports and another 24 ports is necessary (3U) . Hence we need an enclosure rack of 6 * 3 = 36U.

# 6. Patch cords

**Total patch cords wih 0.5 meters=** 90

**Total patch cords wih 5 meters=** 90


# 7. Total Inventory for Building 3 #

* 404.88 meters of monomode fiber cables
* 2700 meters of CAT7 cable
* 88 outlets
* 2 Access Point devices
* 1 Intermediate Cross-Connect
* 2 Horizontal Cross-Connect
* 2 CAT7 48 port Patch Panels
* 2 CAT7 24 port Patch Panel
* 90 patch cords
