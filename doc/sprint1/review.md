RCOMP 2021-2022 Project - Sprint 1 review
=========================================
### Sprint master: 1201623 ###

# 1. Sprint's backlog #
* **T.1.1** Development of a structured cabling project for building 1, encompassing the campus backbone.
* **T.1.2** Development of a structured cabling project for building 2.
* **T.1.3** Development of a structured cabling project for building 3.
* **T.1.4** Development of a structured cabling project for building 4.
# 2. Subtasks assessment #

## 2.1. 1201623 - Structured cable design for building 1 #
### Totally implemented with some issues. ###
Difficulties on getting the number of CPs to install as there is no specific rule for their use.
## 2.2. 1200735 - Structured cable design for building 2 #
### Totally implemented with some issues. ###
The calculation of patch cords were ignored, since we did not know how they were used within cross connects. Other than that, it was Totally implemented with no issues.		
## 2.3. 1191568 - Structured cable design for building 3 #
### Totally implemented with no issues. ###
The most difficult part in the sprint was the calculation about the patch cords since we were not given much information.
## 2.4. 1191706 - Structured cable design for building 4 #
### Totally implemented with some issues ### 
Difficulties understanding how many cables go in and out of a enclosure, and what hardware a enclosure has.
