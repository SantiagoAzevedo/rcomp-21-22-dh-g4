RCOMP 2021-2022 Project - Sprint 1 - Member 1201623 folder
===========================================

This is a view of the private area. The red line represents the fiber cables that will be connected to the ICs and MC.

![Private area](Private Area.PNG)

# Building 1 #

# 1. Plants

## Floor 0 Plant

![Building1_Floor0_Plant](Building1_Floor0_Plant.PNG)

## Floor 1 Plant

![Building1_Floor1_Plant](Building1_Floor1_Plant.PNG)

# 2. Measures

The horizontal dimensions of building 1 are approximately 20 x 30 meters. To calculate the number of outlets, it was followed the instruction "Structured cabling standards specify a minimum of two outlets per work area, and also a ratio of two outlets for each 10 square meters of area.".

## 2.1 Floor 0

### Room 1.0.1 ###

**Top:** 3.05 m

**Side:** 6.54 m

**Area** is approximately 20 m<sup>2</sup> 

**Outlets:** 4

### Room 1.0.2 ###

**Top:** 4.46 m

**Side:** 6.54 m

**Area** is approximately 29,9 m<sup>2</sup>


### Room 1.0.3 ###

**Top:** 4.46 m

**Side:** 6.54 m

**Area** is approximately 29,9 m<sup>2</sup>

**Outlets:** 6 

### Room 1.0.4 ###

**Top:** 6.54 m

**Side:** 6.54 m

**Area** is approximately 44.9 m<sup>2</sup>

**Outlets:** 10

### Room 1.0.5 ###

**Top:** 3.05 m

**Side:** 5 m

**Area** is approximately 15 m<sup>2</sup>

**Outlets:** 4 

### Room 1.0.6 ###

**Top:** 3.05 m

**Side:** 5 m

**Area** is approximately 15 m<sup>2</sup>

**Outlets:** 4

### Room 1.0.7 ###

**Top:** 3.05 m

**Side:** 5 m

**Area** is approximately 15 m<sup>2</sup>

**Outlets:** 4 

### Room 1.0.8 ###

**Top:** 5 m

**Side:** 5 m

**Area** is approximately 25 m<sup>2</sup>

**Outlets:** 6 

### Room 1.0.9 ###

**Top:** 5.8 m

**Side:** 4.69 m

**Area** is approximately 27.2 m<sup>2</sup>

**Outlets:** 6 

### Room 1.0.10 ###

**Top:** 5.8 m

**Side:** 7.14 m

**Area** is approximately 41.4 m<sup>2</sup>

**Outlets:** 10  

## 2.2 Floor 1

### Room 1.1.1 ###

**Top:** 3.1 m

**Side:** 6.6 m

**Area** is approximately 20.46 m<sup>2</sup>

### Room 1.1.2 ###

**Top:** 7.38 m

**Side:** 6.6 m

**Area** is approximately 48.7 m<sup>2</sup>

**Outlets:** 10

### Room 1.1.3 ###

**Top:** 8.2 m

**Side:** 6.6 m

**Area** is approximately 54.1 m<sup>2</sup>

**Outlets:** 12 

### Room 1.1.4 to 1.1.13 ###

**Top:** 2.75 m

**Side:** 5 m

**Area** is approximately 13.75 m<sup>2</sup>

**Outlets:** 4 

### Room 1.1.14 ###

**Top:** 5.7 m

**Side:** 7 m

**Area** is approximately 40.67 m<sup>2</sup>

**Outlets:** 10 

# 3. Cross Connects

## 3.1 Floor 0

A Main Cross Connect, an Intermediate Cross Connect (IC) and a Horizontal Cross Connect (HC) were put in de datacentre room, 1.5 meter above the ground.
Also, because rooms 1.0.4 and 1.0.10 requires 10 outlets each, a Consolidation Point was put there, to minimize the length of necessary cables. This CPs have 4 cables connected to the HC of this floor (redundancy) to prevent any problems with the outlets in these rooms.

### MC ###

Incoming cables: 0

Outgoing cables: 32 

### IC ###

Incoming cables: 8

Outgoing cables: 24

### HC ###

Incoming cables: 8

Outgoing cables: 43 + 8 (other HC)

### Room 1.0.4 CP ###

Incoming cables: 4

Outgoing cables: 10

### Room 1.0.10 CP ###

Incoming cables: 4

Outgoing cables: 10



## 3.2 Floor 1

A Horizontal Cross Connect (HC) was put in room 1.1.1, 2 meters above the ground.
On this floor, there are 3 concentrations of outlets, so it is advisable to use a consolidation point for each: 1 for the rooms 1.1.2 and 1.1.3, 1 for the rooms 1.1.4 to 1.1.13 and the last one for room 1.1.14.
This CPs have 4 cables connected to the HC of this floor (redundancy) to prevent any problems with the outlets in these rooms.

### HC ###

Incoming cables: 8 + 8 (other HC)

Outgoing cables: 13

### Room 1.1.2 CP ###

Incoming cables: 4

Outgoing cables: 22

### Room 1.1.9 CP ###

Incoming cables: 4

Outgoing cables: 40

### Room 1.1.14 CP ###

Incoming cables: 4

Outgoing cables: 10

# 4. Cables

As defined for the whole project, CAT7 cables were used for the Horizontal Cabling (beacuse allows up to 10 Gbps data rate) and Monomode fiber (because monomode optical fibre cables can be much longer than multimode optical fibre cables) for the Backbone Cabling.To calculate the lenght of the cables, it was followed the instructions "When a high number of cables share the same segment of a pathway, measure the segment pathway length and multiply by the number of cables." and "When a high number of cables irradiates from the same point, we can estimate the average cable length and multiply by the number of cables. This will be accurate if the cables length distribution is symmetrical. For instance, we can estimate the average cable length based on the two longest cables and two shortest cables.".

## 4.1 Floor 0

### Backbone cabling (monomode fiber)

* 3 * (1.5 + 6.54 + 2.70 + 1)  meters (from MC to entrance of underfloor cable raceway * 3 ICs exterior to this building)
* 6.1  meters (from MC to IC)
* 9.8 meters (from the IC to the floor above)
* 10.66 meters (from the IC to the HC of this floor)
* 11.24 meters (from the HC of this floor to the floor above)

Total = 73.02 * 8 = 584.16 meters

It is suggested that more cable length be purchased. 

### Horizontal cabling (CAT7)

To best position the access point, one solution will be to create a connection out of room 1.0.7 as you can see in the image above, since placing the AP inside a room can impair the signal transmitted by it. To connect the outlets were respected the suggestions: avoid putting outlets in the middle of the rooms and mainly use the underfloor cable raceways. 

* 1.9 meters (from HC to floor cable passageway)
* 8.04 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.1)
* 3.58 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.3)
* 8.54 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.4)
* 39.54 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.5)
* 36 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.6)
* 32.44 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.7)
* 26.95 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.8)
* 29.99 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.9)
* 35.17 meters (from floor cable passageway entrance in 1.0.2 to near-floor cable raceway in 1.0.10)

#### Room 1.0.1 ####

Closest outlet: 3.08 m

Farthest outlet: 10.79 m 

Average = (10,79 + 3.08) / 2 = 6.935 m 

Cables inside room: 4 outlets * 6.935 = 27.74 m

Cables until room: 8.04 * 4 = 32.16 m

**Total =** 59.9 m

#### Room 1.0.3 ####

Closest outlet: 1.85 m

Farthest outlet: 13.2 m 

Average = (13.2+1.85) / 2 = 7.52 m 

Cables inside room: 6 outlets * 7.52 = 45.12 m

Cables until room: 6 outlets * 3.58 = 21.48 m

**Total =** 66.63 m

#### Room 1.0.4 ####

Closest outlet: 1.75 m

Farthest outlet: 11.84 m 

Average = (11.84 + 1.75) / 2 = 6.795 m 

Cables inside room: 10 outlets * 6.795 = 67.95 m

Cables until room: 8.54 * 4 (CP redundancy) = 34.16 m

**Total =** 102.11 m

#### Room 1.0.5 ####

Closest outlet: 1.52 m

Farthest outlet: 9.35 m 

Average = (1.52 + 9.35) / 2 = 5,475 m 

Cables inside room: 4 outlets * 5,475 = 21.9 m

Cables until room: 39.54 * 4 = 158.1 m

**Total =** 180 m

#### Room 1.0.6 ####

Closest outlet: 1.52 m

Farthest outlet: 6.23 m 

Average = (1.52 + 6.23) / 2 = 3.875 m 

Cables inside room: 4 outlets * 3.875 = 15.5 m

Cables until room: 36 * 4 = 144 m

**Total =** 159.5 m

#### Room 1.0.7 ####

Closest outlet: 1.52 m

Farthest outlet: 9.35 m 

Average = (1.52 + 9.35) / 2 = 5,475 m 

Cables inside room: 4 outlets * 5,475 = 21.9 m

Cables until room: 32.44 * 4 = 129.76 m

**Total =** 151.66 m

#### Room 1.0.8 ####

**Total =** 36.95 m

#### Room 1.0.8 ####

Closest outlet: 1.3 m

Farthest outlet: 8.45 m 

Average = (1.3 + 8.45) / 2 = 4.875 m 

Cables inside room: 6 outlets * 4.875 = 29.25 m

Cables until room: 26.95 * 6 = 161.7 m

**Total =** 190.95 m

#### Room 1.0.9 ####

Closest outlet: 0.67 m

Farthest outlet: 6.8 m 

Average = (0.67 + 6.8) / 2 = 3.735 m 

Cables inside room: 6 outlets * 3.735 = 22.41 m

Cables until room: 29.99 * 6 = 179.94 m

**Total =** 202.35 m

#### Room 1.0.10 ####

Closest outlet: 0.44 m

Farthest outlet: 15.75 m 

Average = (0.44 + 15.75) / 2 = 8.095 m 

Cables inside room: 10 outlets * 8.095 = 80.95 m

Cables until room: 35.17 * 4 (CP redundancy) = 140.68 m

**Total =** 221.63 m

### Total lenght of the cables = 1371.68 meters

It is suggested that more cable length be purchased. 

## 4.2 Floor 1

### Backbone cabling (monomode fiber) ###

* 3.9 meters (-From IC- from floor cable passageway to HC )
* 4.9 meters (-From HC- from floor cable passageway to HC)

Total = 8.8 * 8 = 70.4 meters

It is suggested that more cable length be purchased. 

### Horizontal cabling (CAT7) ###

Not to have many cables through the walls in the rooms and consequently obstruct the same, the outlets of the same working area enter the same place and descend almost to the floor to go through the rest of the room, as happened on the lower floor but in reverse. The different passageways of the cables were differentiated in the plant, with different colors to distinguish between the cables that pass through the removable dropped ceiling and the cables that go through the walls. As there is a removable dropped ceiling, the installation of the access point is facilitated, so it is enough to connect a cable to the HC.

* 1.5 meters (from HC to ceiling cable passageway)
* 3.73 meters (from ceiling cable passageway entrance in 1.1.1 to near-floor cable raceway in 1.1.2)
* 10.35 meters (from ceiling cable passageway entrance in 1.1.1 to near-floor cable raceway in 1.1.3)
* 17.18 meters (from ceiling cable passageway entrance in 1.1.1 to the second CP)
* 14 meters (from second CP to ceiling cable passageway entrance in 1.1.4)
* 14.8 meters (from second CP to ceiling cable passageway entrance in 1.1.5)
* 26.4 meters (from second CP to ceiling cable passageway entrance in 1.1.6)
* 38.4 meters (from second CP to ceiling cable passageway entrance in 1.1.7)
* 50.4 meters (from second CP to ceiling cable passageway entrance in in 1.1.8)
* 14.8 meters (from second CP to ceiling cable passageway entrance in 1.1.10)
* 26.4 meters (from second CP to ceiling cable passageway entrance in in 1.1.11)
* 38.4 meters (from second CP to ceiling cable passageway entrance in 1.1.12)
* 50.4 meters (from second CP to ceiling cable passageway entrance in 1.1.13)

#### Room 1.1.2 ####

Closest outlet: 2.55 m

Farthest outlet: 16.61 m 

Average = (2.55 + 16.61) / 2 = 9.58 m 

Cables inside room: 10 outlets * 9.58 = 95.8 m

Cables until room: 3.73 * 4 (CP redundancy) = 14.92 m

**Total =** 110.72 m

#### Room 1.1.3 ####

Closest outlet: 15.13 m

Farthest outlet: 34.28 m 

Average = (15.13 + 34.28) / 2 = 24.705 m 

Cables inside room: 12 outlets * 24.705 = 296.46 m

**Total =** 296.46 m

#### Consolidation point 2 ####

**Total =** 68.72 m

#### Room 1.1.4 ####

Closest outlet: 2 m

Farthest outlet: 13.75 m 

Average = (2 + 13.75) / 2 = 7.875 m 

Cables inside room: 4 outlets * 7.875 = 31.5 m

Cables until room: 3.5 * 4 = 14 m

**Total =** 45.5 m

#### Room 1.1.5 ####

Closest outlet: 2 m

Farthest outlet: 9.2 m 

Average = (2 + 9.2) / 2 = 5.6 m 

Cables inside room: 4 outlets * 5.6 = 22.4 m

Cables until room: 3.7 * 4 = 14.8 m

**Total =** 37.2 m

#### Room 1.1.6 ####

Closest outlet: 2 m

Farthest outlet: 9.2 m 

Average = (2 + 9.2) / 2 = 5.6 m 

Cables inside room: 4 outlets * 5.6 = 22.4 m

Cables until room: 6.6 * 4 = 26.4 m

**Total =** 48.8 m

#### Room 1.1.7 ####

Closest outlet: 2 m

Farthest outlet: 9.2 m 

Average = (2 + 9.2) / 2 = 5.6 m 

Cables inside room: 4 outlets * 5.6 = 22.4 m

Cables until room: 9.6 * 4 = 38.4 m

**Total =** 60.8 m

#### Room 1.1.8 ####

Closest outlet: 2 m

Farthest outlet: 13.75 m 

Average = (2 + 13.75) / 2 = 7.875 m 

Cables inside room: 4 outlets * 7.875 = 31.5 m

Cables until room: 12.6 * 4 = 50.4 m

**Total =** 81.9 m

#### Room 1.1.9 ####

Closest outlet: 1.9 m

Farthest outlet: 10.85 m 

Average = (1.9 + 10.85) / 2 = 6.375 m 

Cables inside room: 4 outlets * 6.375 = 25.5 m

Cables until room: -- (The CP is in this room)

**Total =** 25.5 m

#### Room 1.1.10 ####

Closest outlet: 2 m

Farthest outlet: 9.2 m 

Average = (2 + 9.2) / 2 = 5.6 m 

Cables inside room: 4 outlets * 5.6 = 22.4 m

Cables until room: 3.7 * 4 = 14.8 m

**Total =** 37.2 m

#### Room 1.1.11 ####

Closest outlet: 2 m

Farthest outlet: 9.2 m 

Average = (2 + 9.2) / 2 = 5.6 m 

Cables inside room: 4 outlets * 5.6 = 22.4 m

Cables until room: 6.6 * 4 = 26.4 m

**Total =** 48.8 m

#### Room 1.1.12 ####

Closest outlet: 2 m

Farthest outlet: 9.2 m 

Average = (2 + 9.2) / 2 = 5.6 m 

Cables inside room: 4 outlets * 5.6 = 22.4 m

Cables until room: 9.6 * 4 = 38.4 m

**Total =** 60.8 m

#### Room 1.1.13 ####

Closest outlet: 2 m

Farthest outlet: 13.75 m 

Average = (2 + 13.75) / 2 = 7.875 m 

Cables inside room: 4 outlets * 7.875 = 31.5 m

Cables until room: 12.6 * 4 = 50.4 m

**Total =** 81.9 m

#### Access-point ####

**Total =** 25.78 m 

#### Room 1.1.14 ####

Closest outlet: 2.5 m

Farthest outlet: 22.96 m 

Average = (2.5 + 22.96) / 2 = 12.73 m 

Cables inside room: 10 outlets * 12.73 = 127.3 m

Cables until room: 36.08 * 4 = 144.32 m

**Total =** 271.62 m

### Total lenght of the cables = 1233 meters

It is suggested that more cable length be purchased. 

# 5. Enclosures
To calculate the enclosure's size, it was followed the instruction "...once we know the U units space required for the patch panels is (S), we can reach a (6 x S) U units size for the enclosure.".

## 5.1 Floor 0

### MC ###

A fiber patch panel of 48 ports is necessary (2U). Hence we need an enclosure rack of 12U.

### IC ###

A fiber patch panel of 48 ports is necessary (2U). Hence we need an enclosure rack of 12U.

### HC ###

A CAT7 patch panel of 48 ports (2U) and a fiber patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 18U.

### ROOM 1.0.4 CP ###

A CAT7 patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 6U.

### ROOM 1.0.10 CP ###

A CAT7 patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 6U.


## 5.2 Floor 1

### HC ###

A CAT7 patch panel of 48 ports is necessary (2U) and a fiber patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 18U.

### ROOM 1.1.2 CP ###

A CAT7 patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 6U.

### ROOM 1.1.9 CP ###

A CAT7 patch panel of 48 ports is necessary (2U). Hence we need an enclosure rack of 12U.

### ROOM 1.1.14 CP ###

A CAT7 patch panel of 24 ports is necessary (1U). Hence we need an enclosure rack of 6U.

# 6. Patch cords

**Total patch cords wih 0.5 meters=** 212

**Total patch cords wih 5 meters=** 126


## Total of this building ##

### Backbone cabling (monomode fiber) ###

A total of **654.56 meters of monomode fiber** cables will be requested.

### Horizontal cabling (CAT7) ###

A total of **2605 meters of CAT7** cable will be requested.

## Total lenght of connections between buildings ##

To calculate the incoming and outgoing cables, it was followed the instruction "For the sake of date path redundancy, additional cable links should also be installed between same level cross connect points, for instance, connecting different IC and connecting different HC".

### Backbone cabling (monomode fiber) ###

#### MC - other 3 ICs (since the lenght for the IC of this building was already calculated in 4.1)

A total of **2513.92 meters of monomode fiber** cables will be requested to connect the others IC to the MC.

#### Building 1 IC - Building 2 IC

8.3 (from entrance of building 1 to IC) + 4.05 (from entrance of building 2 to IC) + 40 (Technical ditch) = **52.35 meters**

#### Building 2 IC - Building 3 IC

4.05 (from entrance of building 2 to IC) + 31.86 (from entrance of building 3 to IC) + 57 (Technical ditch) = **92.91 meters**

#### Building 3 IC - Building 4 IC

31.86 (from entrance of building 3 to IC) + 23.5 (from entrance of building 4 to IC) + 64.5 (Technical ditch) = **119.86 meters**

**To have redundancy:** 265.12 * 8

A total of **2120.96 meters of monomode fiber** cables will be requested to connect the ICs between them.

# Total Inventory for Building 1 #

* 654.56 meters of monomode fiber cables 
* 2605 meters of CAT7 cable 
* 126 outlets
* 2 Access Point devices
* 1 Main Cross-Connect
* 1 Intermediate Cross-Connect
* 2 Horizontal Cross-Connect
* 5 Consolidation Point
* 3 Enclosure Racks of 6U
* 3 Enclosure Racks of 12U
* 3 Enclosure Racks of 18U
* 2 Fiber 24 port Patch Panel
* 2 Fiber 48 port Patch Panel
* 3 CAT7 48 port Patch Panels
* 4 CAT7 24 port Patch Panel
* 338 patch cords


