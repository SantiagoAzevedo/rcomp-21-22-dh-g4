RCOMP 2021-2022 Project repository template
===========================================
# 1. Team members (update this information please) #
  * 1191568 - {Fábio Girão} 
  * 1191706 - {João Violante} 
  * 1200735 - {Rui Rocha} 
  * 1201623 - {Santiago Azevedo}  


# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)

